from mpi4py import MPI
import sys
import os
import platform
import subprocess
import time
import numpy as np
import pandas as pd
from opf import util
from opf import AcNode
from opf import DcNode
from opf import NetworkDef
# import pydevd_pycharm


def main(net_path, ruler, integrate_res):

    ruler = int(ruler)
    comm = MPI.COMM_WORLD   # intracommunicator
    size = comm.Get_size()  # will get the total number of processes
    rank = comm.Get_rank()  # will get the "name" of each process

    # if rank == ruler:
    #     port_mapping = [63096]
    #     pydevd_pycharm.settrace('localhost', port=port_mapping[0], stdoutToServer=True, stderrToServer=True)

    net = NetworkDef.NetworkDef(net_path, global_id=rank, ruler=ruler)
    identifier = "_"  # todo

    # validate mpi execution size
    if size != net.global_node_count:
        if rank == ruler:
            print("\nERROR: WRONG NUMBER OF MPI PROCESSES CALLED:\n")
            print("{} processes called, while network requires {}".format(size, net.num_nodes))
            sys.stdout.flush()
        return

    # 1. Node initialization
    if net.node_type == util.AC_NODE:
        node = AcNode.AcNode(
            integrate_res=integrate_res,
            net=net,
            comm=comm
        )
    elif net.node_type == util.DC_NODE:
        node = DcNode.DcNode(
            integrate_res=integrate_res,
            net=net,
            comm=comm
        )
    else:
        raise RuntimeError("Invalid node type")

    # Ensure everything is initialized
    comm.Barrier()

    global_convergence = False
    while node.t < net.max_iter_t and not global_convergence:
        if rank == ruler:
            util.log("t = {}".format(node.t))
            sys.stdout.flush()

        # set global iteration t for timers
        # start timing total execution time
        node.update_timing_iteration()
        node.timers['total'].start()

        # 2. Local solution to qp optimization
        #    Increments t before storing results
        node.update_clone_variables()

        # 3. Exchange clone vars to update net vars
        node.update_net_variables()

        # todo handling of communication failure

        # 4. Dual Variable Update
        node.update_dual_variables()

        # 5. Check for global convergence
        global_convergence = node.check_global_convergence()

        # stop total execution time for this iteration
        node.timers['total'].stop()

    # 6. Save results
    gather_and_save_results(node, global_convergence, identifier)

    return 0


def gather_and_save_results(node, global_convergence, identifier):
    if node.net.global_id == node.net.ruler:
        util.log("Gathering results...")

    # net voltages (note t-1 was final solution)
    # received as (global_node_id, t, v_index)
    ave_v = np.ascontiguousarray(node.ave_v[0:node.t, node.net.node_id, :])
    combined_ave_v = np.zeros((node.net.global_node_count, node.t, 2))
    node.comm.Gather(ave_v, combined_ave_v, root=node.net.ruler)

    # generated power
    generation = np.ascontiguousarray(node.local_sol_gen[0:node.t, :])
    combined_generation = np.zeros((node.net.global_node_count, node.t, 2))
    node.comm.Gather(generation, combined_generation, root=node.net.ruler)

    # objective function
    objective_fun = np.array(node.local_sol[0:node.t, -2])
    combined_objective_fun = np.zeros((node.net.global_node_count, node.t, 1))
    node.comm.Gather(objective_fun, combined_objective_fun, root=node.net.ruler)

    # gather converter data
    # collect data from DC nodes, so that power losses are available
    global_converter_data = None
    if node.net.net_type == util.NET_AC_DC:
        req = list()
        tag = 4
        global_converter_data = np.zeros((node.net.global_converter_count, node.t, 3))
        for converter_id, dc_node_id in enumerate(node.net.dc_nodes_with_converters):
            dc_node_global_id = node.net.get_dc_global_id(dc_node_id)
            if node.net.global_id == dc_node_global_id:
                # 1. send value
                req.append(node.comm.Isend(
                    np.ascontiguousarray(node.local_sol_converter[0:node.t, converter_id, :]),
                    dest=node.net.ruler,
                    tag=tag
                ))  # non-blocking

            if node.net.global_id == node.net.ruler:
                # 2. receive values
                req.append(node.comm.Irecv(
                    global_converter_data[converter_id, :, :],
                    source=dc_node_global_id,
                    tag=tag
                ))  # non-blocking
        MPI.Request.Waitall(req)

    if node.net.global_id == node.net.ruler:
        util.log("Results gathered.")
        util.log("Overview:")
        util.log("Final voltages (re, im): \n{}\n".format(combined_ave_v[:, -1, :]))
        util.log("Final generation (P, Q): \n{}\n".format(combined_generation[:, -1, :]))
        util.log("Total generation (P, Q): {}".format(np.sum(combined_generation[:, -1, :], axis=0)))
        util.log("Total demand (P): {}".format(node.net.global_load))
        util.log("Convergence conditions: {}".format(node.net.v_conv))
        util.log("Maximum iterations: {}".format(node.net.max_iter_t))
        util.log("Iterations required: {}".format(node.t))
        util.log("Convergence reached: {}".format(global_convergence))

        util.log("\n Timing Summary:")
        for name, timer in node.timers.items():
            util.log("Total {} time {}".format(name, timer.total()))
            util.log("\t Avg {}".format(timer.avg()))
            util.log("\t Std {}".format(timer.std()))
            util.log("\t Min {}".format(timer.min()))
            util.log("\t Max {}".format(timer.max()))

        util.log("Saving the results as .xlsx...")

        # Creating folder if it doesn't exist
        dir_name = os.path.splitext(node.net.config_file_path)[0].replace('networks', 'results')
        try:
            os.makedirs(dir_name)
            print("Directory ", dir_name, " created, results will be stored there... ")
        except FileExistsError:
            print("Directory ", dir_name, " already exists, results will be stored there...")

        # write results file
        with pd.ExcelWriter("{}/{}results.xlsx".format(dir_name, identifier)) as writer:
            write_ac_network_data(writer, node, combined_ave_v, combined_generation, global_converter_data)
            write_dc_network_data(writer, node, combined_ave_v, combined_generation, global_converter_data)
            add_converter_data(writer, node, global_converter_data)
            add_convergence_data(writer, node)

        # saving identifier
        with open("temp_identifier.txt", "w") as text_file:
            text_file.write(identifier)
        with open("temp_dirName.txt", "w") as text_file:
            text_file.write(dir_name)
        print("\nResults saved.\n")

        # calling matlab
        print("Calling Matlab...\n")
        if platform.system() == "Windows":
            # os.system("matlab -nodesktop -nosplash -r \"plot_data\"")
            subprocess.Popen(
                "matlab -nodesktop -nosplash -r \"plot_data\"",
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE)

            # once matlab completes, all files should be deleted
            # timeout after 1 min
            counter = 0
            while any("temp_" in s for s in os.listdir()):
                if counter > 20:
                    print("Matlab operation has exceeded 1 min, timing out...")
                    break
                else:
                    time.sleep(3)
                    counter += 1
            # os.system("del temp_*") # done by matlab script
        else:
            os.system("/Applications/MATLAB_R2018b.app/bin/matlab -nodesktop -nosplash -r \"plot_data\"")
            # os.system("rm -f temp_*")

        print("\n\nFinished.\n\n")
        sys.stdout.flush()


def write_ac_network_data(writer, node, combined_ave_v, combined_generation, global_converter_data):
    if node.net.ac_node_count > 0:
        ac_nodes = slice(0, node.net.ac_node_count)
        # compute voltages in polar form
        v_mag = np.sqrt(np.sum(combined_ave_v[ac_nodes, :, :] ** 2, axis=2))
        v_ang = np.arctan(combined_ave_v[ac_nodes, :, 1] / combined_ave_v[ac_nodes, :, 0]) * 180 / np.pi

        pd.DataFrame(np.transpose(combined_ave_v[ac_nodes, :, 0]))\
            .to_excel(writer, sheet_name="ac_v_re")

        pd.DataFrame(np.transpose(combined_ave_v[ac_nodes, :, 1]))\
            .to_excel(writer, sheet_name="ac_v_im")

        pd.DataFrame(np.transpose(v_mag))\
            .to_excel(writer, sheet_name="ac_v_mag")

        pd.DataFrame(np.transpose(v_ang))\
            .to_excel(writer, sheet_name="ac_v_ang")

        pd.DataFrame(np.transpose(combined_generation[ac_nodes, :, 0]))\
            .to_excel(writer, sheet_name="ac_p_gen")

        pd.DataFrame(np.transpose(combined_generation[ac_nodes, :, 1]))\
            .to_excel(writer, sheet_name="ac_q_gen")

        pd.DataFrame(np.ones(node.t) * node.net.ac_load[0])\
            .to_excel(writer, sheet_name="ac_p_load")

        pd.DataFrame(np.ones(node.t) * node.net.ac_load[1])\
            .to_excel(writer, sheet_name="ac_q_load")


def write_dc_network_data(writer, node, combined_ave_v, combined_generation, global_converter_data):
    if node.net.dc_node_count > 0:
        dc_nodes = slice(node.net.ac_node_count, node.net.global_node_count)

        pd.DataFrame(np.transpose(combined_ave_v[dc_nodes, :, 0]))\
            .to_excel(writer, sheet_name="dc_v")

        pd.DataFrame(np.transpose(combined_generation[dc_nodes, :, 0]))\
            .to_excel(writer, sheet_name="dc_p_gen")

        pd.DataFrame(np.ones(node.t) * node.net.dc_load[0])\
            .to_excel(writer, sheet_name="dc_p_load")


def add_converter_data(writer, node, global_converter_data):
    if node.net.net_type == util.NET_AC_DC:

        pd.DataFrame(np.transpose(global_converter_data[:, :, 0]))\
            .to_excel(writer, sheet_name="conv_p")

        pd.DataFrame(np.transpose(global_converter_data[:, :, 1]))\
            .to_excel(writer, sheet_name="conv_q")

        pd.DataFrame(np.transpose(global_converter_data[:, :, 2]))\
            .to_excel(writer, sheet_name="conv_p_loss")


def add_convergence_data(writer, node):
    convergence_data = [
        node.net_var_convergence_sum,
        np.ones(node.t) * node.net.global_node_count
    ]

    pd.DataFrame(np.transpose(convergence_data)) \
        .to_excel(writer, sheet_name="convergence")


if __name__ == "__main__":
    if len(sys.argv) < 4:
        raise RuntimeError("Insufficient inputs provided to Main.py", str(sys.argv))
    main(sys.argv[1], sys.argv[2], sys.argv[3])
