# -*- coding: utf-8 -*-
"""acs_hybrid_opf -> start.py

Main entry point to the ACS Hybrid network DOPF solver.
This file will prompt the user for the appropriate inputs
and then initiate the required processes according to the
selected case.
"""
import os
import sys
from opf import NetworkDef


def main():
    print("\n\t\tWELCOME TO THE USER INTERFACE FOR \"FEN-DOPF\"")
    print("\t\t--------------------------------------------")
    print("\t\t        created by Miguel Berbegal\n")

    net_def = prompt_network('networks')
    ruler = -1 + prompt_integer_input(
        question="What entity should assume the role of ruler?",
        valid_range=range(1, net_def.global_node_count + 1)
    )

    integrate_res = prompt_yes_no_input(
        question="Would you like to integrate RES ESS and CL?"
    )

    run_on_hpc = prompt_yes_no_input(
        question="Are you executing this in the swarm HPC cluster?"
    )

    if run_on_hpc:
        num_hosts = prompt_integer_input(
            question="How many swarm nodes would you like to use?",
            valid_range=range(1, 5)  # end value not included, so 1-4
        )

    command = ["mpiexec", "-n", str(net_def.global_node_count)]

    if run_on_hpc:
        command.append("-hosts")
        if num_hosts == 1:
            command.append("swarm1")    # swarm1 always first cause there is matlab installed
        elif num_hosts == 2:
            command.append("swarm1,swarm0")
        elif num_hosts == 3:
            command.append("swarm1,swarm0,swarm2")
        elif num_hosts == 4:
            command.append("swarm1,swarm0,swarm2,swarm3")
        command.append("python3.6")
    else:
        command.append(sys.executable)  # use the current python executable (supports virtual env)

    command.append("Main.py")
    command.append(net_def.config_file_path)
    command.append(str(ruler))
    command.append(str(integrate_res))

    command = " ".join(command)
    print("\nLoading case...\n")
    print(command)  # printing the command so that the user knows what is being executed
    os.system(command)


def prompt_network(network_dir):
    networks = []
    print("Please select a network from the following list:")
    with os.scandir(network_dir) as it:
        i = 0
        for entry in it:
            if entry.is_dir():
                print("[{}]: \t {}".format(i, entry.name))
                networks.append(entry)
                i += 1

    if len(networks) == 0:
        raise RuntimeError("Please ensure there are network definition files in the networks directory.")

    network_id = prompt_integer_input(
        question="Desired network".format(0, len(networks)),
        valid_range=range(0, len(networks))
    )

    # return loaded network definition
    return NetworkDef.NetworkDef(networks[network_id].path)


def prompt_integer_input(question, valid_range):
    ans = input("{} ({} to {}):_ ".format(question, valid_range.start, valid_range.stop - 1))
    while not is_int(ans) or int(ans) not in valid_range:
        ans = input("\tNot to be cheeky, but please stop trolling \
        and enter an integer between {} and {}:_ ".format(valid_range.start, valid_range.stop - 1))

    return int(ans)


def prompt_yes_no_input(question):
    ans = input("{} (yes or no):_ ".format(question)).lower()
    while ans != "yes" and ans != "no":
        ans = input("\tCome on now, a simple yes or no will do:_ ").lower()

    return ans == 'yes'


def is_int(string):  # checking if the string is an integer
    try:
        val = int(string)
        return True
    except ValueError:
        return False


if __name__ == "__main__":
    main()
