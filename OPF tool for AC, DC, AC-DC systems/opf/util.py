
# ac specific state constants
ZAC_V_I_RE = 'V_i_re'
ZAC_V_I_IM = 'V_i_im'
ZAC_I_QI_RE = 'I_qi_re'
ZAC_I_QI_IM = 'I_qi_im'
ZAC_P_QI = 'P_qi'
ZAC_Q_QI = 'Q_qi'
ZAC_P_GEN_Q = 'P_gen_q'
ZAC_Q_GEN_Q = 'Q_gen_q'
ZAC_P_C_K = 'P_c_k'
ZAC_Q_C_K = 'Q_c_k'
ZAC_P_INJ_Q = 'P_inj_q'
ZAC_Q_INJ_Q = 'Q_inj_q'
ZAC_I_Q_RE = 'I_q_re'
ZAC_I_Q_IM = 'I_q_im'
ZAC_V_Q_RE = 'V_q_re'
ZAC_V_Q_IM = 'V_q_im'

# dc specific state constants
ZDC_V_I = 'V_i'
ZDC_I_QI = 'I_qi'
ZDC_P_QI = 'P_qi'
ZDC_P_GEN_Q = 'P_gen_q'
ZDC_P_C_K = 'P_c_k'
ZDC_Q_C_K = 'Q_c_k'
ZDC_PL_C_K = 'PL_c_k'
ZDC_P_INJ_Q = 'P_inj_q'
ZDC_I_Q = 'I_q'
ZDC_V_Q = 'V_q'

# AC Taylor constants
TAC_V_RE = 'v_re'
TAC_V_IM = 'v_im'
TAC_I_RE = 'i_re'
TAC_I_IM = 'i_im'
TAC_P = 'p'
TAC_Q = 'q'
TAC_GEN_P = 'p_gen'
TAC_GEN_Q = 'q_gen'
TAC_P_C_K = 'p_c_k'
TAC_Q_C_K = 'q_c_k'

# DC Taylor constants
TDC_V = 'v'
TDC_I = 'i'
TDC_P = 'p'
TDC_GEN_P = 'p_gen'
TDC_P_C_K = 'p_c_k'
TDC_PL_C_K = 'pl_c_k'
TDC_Q_C_K = 'q_c_k'


# Limit constants
L_V_MIN = 'v_min'
L_V_MAX = 'v_max'
L_I_FLOW_MAX = 'i_flow_max'
L_S_FLOW_MAX = 's_flow_max'
L_I_INJ_MAX = 'i_inj_max'
L_P_INJ_MAX = 'p_inj_max'
L_Q_INJ_MAX = 'q_inj_max'
L_S_INJ_MAX = 's_inj_max'
L_P_GEN_MIN = 'p_gen_min'
L_Q_GEN_MIN = 'q_gen_min'
L_P_GEN_MAX = 'p_gen_max'
L_Q_GEN_MAX = 'q_gen_max'
L_S_GEN_MAX = 's_gen_max'
L_P_CONV_MAX = 'p_conv_max'
L_Q_CONV_MAX = 'q_conv_max'
L_S_CONV_MAX = 's_conv_max'

# dc specific limits
L_P_FLOW_MAX = 'p_flow_max'

# network type
NET_AC = 'ac_network'
NET_DC = 'dc_network'
NET_AC_DC = 'ac_dc_network'

# NODE_TYPE
AC_NODE = 'ac_node'
DC_NODE = 'dc_node'


def log(msg):
    print(msg)