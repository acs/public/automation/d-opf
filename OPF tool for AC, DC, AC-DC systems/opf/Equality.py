from . import util
from . import AcNode
from . import DcNode
from . import Node


class Equality:

    # Base class for equality constraints
    # They should be initialized only from the Node's init_equalities() function
    def __init__(self, name, node: Node, num_rows=1):
        self.name = name
        self.node = node
        self.rows = range(len(self.node.A_eq), len(self.node.A_eq) + num_rows)

        # Pre allocate rows in A_eq and b_eq
        for i in self.rows:
            self.node.A_eq.append([0] * self.node.num_state_vars)
            self.node.b_eq.append(0)

    # template function
    def update(self): pass


# P and Q balance between generation, demand and injections at this node (q) [green]
class ACNodePowerBalanceEq(Equality):

    def __init__(self, name, node: AcNode):
        super().__init__(name=name, node=node, num_rows=2)

        # Constant terms for this equality
        # Conservation of real power at this node
        self.node.b_eq[self.rows[0]] = self.node.net.load[0]  # P demanded at this node
        self.node.A_eq[self.rows[0]][self.node.z[util.ZAC_P_GEN_Q][0].row] = 1              # P_gen
        self.node.A_eq[self.rows[0]][self.node.z[util.ZAC_P_INJ_Q][0].row] = -1             # P_inj

        self.node.b_eq[self.rows[1]] = self.node.net.load[1]  # Q demanded at this node
        self.node.A_eq[self.rows[1]][self.node.z[util.ZAC_Q_GEN_Q][0].row] = 1              # Q_gen
        self.node.A_eq[self.rows[1]][self.node.z[util.ZAC_Q_INJ_Q][0].row] = -1             # Q_inj

        if self.node.net.net_type == util.NET_AC_DC:
            for state_var in self.node.z[util.ZAC_P_C_K]:
                self.node.A_eq[self.rows[0]][state_var.row] = 1  # P converter (flow to ac side)

            for state_var in self.node.z[util.ZAC_Q_C_K]:
                self.node.A_eq[self.rows[1]][state_var.row] = 1  # Q converter (injection on ac side)

    def update(self):
        super().update()
        # no variable terms, nothing to do here


# KCL at this node (q) [red]
class ACNodeCurrentEq(Equality):

    def __init__(self, name, node: AcNode):
        super().__init__(name=name, node=node, num_rows=2)

        # Constant terms for this equality
        # This node's terms (q)
        self.node.A_eq[self.rows[0]][self.node.z[util.ZAC_I_Q_RE][0].row] = -1
        self.node.A_eq[self.rows[0]][self.node.z[util.ZAC_V_Q_RE][0].row] = self.node.net.gq[self.node.net.node_id]
        self.node.A_eq[self.rows[0]][self.node.z[util.ZAC_V_Q_IM][0].row] = -self.node.net.bq[self.node.net.node_id]

        self.node.A_eq[self.rows[1]][self.node.z[util.ZAC_I_Q_IM][0].row] = -1
        self.node.A_eq[self.rows[1]][self.node.z[util.ZAC_V_Q_RE][0].row] = self.node.net.bq[self.node.net.node_id]
        self.node.A_eq[self.rows[1]][self.node.z[util.ZAC_V_Q_IM][0].row] = self.node.net.gq[self.node.net.node_id]

        # node i terms
        for j in range(self.node.net.n2q):
            i_id = self.node.z[util.ZAC_V_I_RE][j].node_id
            g_qi = self.node.net.gq[i_id]  # conductance between this node (q) and neighbouring node (i)
            b_qi = self.node.net.bq[i_id]  # susceptance between this node (q) and neighbouring node (i)

            self.node.A_eq[self.rows[0]][self.node.z[util.ZAC_V_I_RE][j].row] = g_qi   # [V_i,re] red, real current injection
            self.node.A_eq[self.rows[0]][self.node.z[util.ZAC_V_I_IM][j].row] = -b_qi  # [V_i,im] red, real current injection

            self.node.A_eq[self.rows[1]][self.node.z[util.ZAC_V_I_RE][j].row] = b_qi  # [V_i,re] red, imaginary current inj
            self.node.A_eq[self.rows[1]][self.node.z[util.ZAC_V_I_IM][j].row] = g_qi  # [V_i,im] red, imaginary current inj

    def update(self):
        super().update()
        # no variable terms, nothing to do here


# Branch current flow between this node (q) and neighbouring nodes (i) [blue]
class ACBranchCurrentEq(Equality):

    def __init__(self, name, node: AcNode):
        super().__init__(name=name, node=node, num_rows=2 * node.net.n2q)

        # Constant terms for this equality
        for j in range(self.node.net.n2q):
            i_id = self.node.z[util.ZAC_V_I_RE][j].node_id
            g_qi = self.node.net.gq[i_id]  # conductance between this node (q) and neighbouring node (i)
            b_qi = self.node.net.bq[i_id]  # susceptance between this node (q) and neighbouring node (i)
            row_re = self.rows[j]
            row_im = self.rows[j] + self.node.net.n2q

            # real current flow from node q to i
            self.node.A_eq[row_re][self.node.z[util.ZAC_V_I_RE][j].row] = g_qi
            self.node.A_eq[row_re][self.node.z[util.ZAC_V_I_IM][j].row] = -b_qi
            self.node.A_eq[row_re][self.node.z[util.ZAC_I_QI_RE][j].row] = -1
            self.node.A_eq[row_re][self.node.z[util.ZAC_V_Q_RE][0].row] = -g_qi
            self.node.A_eq[row_re][self.node.z[util.ZAC_V_Q_IM][0].row] = b_qi

            # imaginary current flow from node q to i
            self.node.A_eq[row_im][self.node.z[util.ZAC_V_I_RE][j].row] = b_qi
            self.node.A_eq[row_im][self.node.z[util.ZAC_V_I_IM][j].row] = g_qi
            self.node.A_eq[row_im][self.node.z[util.ZAC_I_QI_IM][j].row] = -1
            self.node.A_eq[row_im][self.node.z[util.ZAC_V_Q_RE][0].row] = -b_qi
            self.node.A_eq[row_im][self.node.z[util.ZAC_V_Q_IM][0].row] = -g_qi

    def update(self):
        super().update()
        # no variable terms, nothing to do here


# Branch power flow between this node (q) and neighbouring nodes (i) [orange]
class ACBranchPowerEq(Equality):

    def __init__(self, name, node: AcNode):
        super().__init__(name=name, node=node, num_rows=2 * node.net.n2q)

        # Constant terms for this equality
        for j in range(self.node.net.n2q):
            self.node.A_eq[self.rows[j]][self.node.z[util.ZAC_P_QI][j].row] = -1
            self.node.A_eq[self.rows[j] + self.node.net.n2q][self.node.z[util.ZAC_Q_QI][j].row] = -1

    def update(self):
        super().update()

        # Variable terms for this equality
        for j in range(self.node.net.n2q):
            i_id = self.node.z[util.ZAC_V_I_RE][j].node_id
            g_qi = self.node.net.gq[i_id]  # conductance between this node (q) and neighbouring node (i)
            b_qi = self.node.net.bq[i_id]  # susceptance between this node (q) and neighbouring node (i)
            row_re = self.rows[j]
            row_im = self.rows[j] + self.node.net.n2q

            # taylor expansion points
            t_i_re = self.node.taylor[util.TAC_V_RE][self.node.x, i_id]
            t_i_im = self.node.taylor[util.TAC_V_IM][self.node.x, i_id]
            t_q_re = self.node.taylor[util.TAC_V_RE][self.node.x, self.node.net.node_id]
            t_q_im = self.node.taylor[util.TAC_V_IM][self.node.x, self.node.net.node_id]

            # real coefficients
            a1 = g_qi * t_i_re - b_qi * t_i_im - 2 * g_qi * t_q_re
            a2 = b_qi * t_i_re + g_qi * t_i_im - 2 * g_qi * t_q_im
            a3 = g_qi * t_q_re + b_qi * t_q_im
            a4 = -b_qi * t_q_re + g_qi * t_q_im
            a5 = g_qi * (t_q_re ** 2 + t_q_im ** 2) - t_i_re * a3 - t_i_im * a4

            # imaginary coefficients
            c1 = -b_qi * t_i_re - g_qi * t_i_im + 2 * b_qi * t_q_re
            c2 = g_qi * t_i_re - b_qi * t_i_im + 2 * b_qi * t_q_im
            c3 = -b_qi * t_q_re + g_qi * t_q_im
            c4 = -g_qi * t_q_re + -b_qi * t_q_im
            c5 = -b_qi * (t_q_re ** 2 + t_q_im ** 2) - t_i_re * c3 - t_i_im * c4

            # update equalities
            self.node.A_eq[row_re][self.node.z[util.ZAC_V_I_RE][j].row] = a3
            self.node.A_eq[row_re][self.node.z[util.ZAC_V_I_IM][j].row] = a4
            self.node.A_eq[row_re][self.node.z[util.ZAC_V_Q_RE][0].row] = a1
            self.node.A_eq[row_re][self.node.z[util.ZAC_V_Q_IM][0].row] = a2
            self.node.b_eq[row_re] = -a5

            # update equalities
            self.node.A_eq[row_im][self.node.z[util.ZAC_V_I_RE][j].row] = c3
            self.node.A_eq[row_im][self.node.z[util.ZAC_V_I_IM][j].row] = c4
            self.node.A_eq[row_im][self.node.z[util.ZAC_V_Q_RE][0].row] = c1
            self.node.A_eq[row_im][self.node.z[util.ZAC_V_Q_IM][0].row] = c2
            self.node.b_eq[row_im] = -c5


# Power injection equations for this node [purple]
class ACNodePowerEq(Equality):

    def __init__(self, name, node: AcNode):
        super().__init__(name=name, node=node, num_rows=2)

        # Constant terms for this equality
        # This node's terms (q)
        self.node.A_eq[self.rows[0]][self.node.z[util.ZAC_P_INJ_Q][0].row] = -1
        self.node.A_eq[self.rows[1]][self.node.z[util.ZAC_Q_INJ_Q][0].row] = -1

    def update(self):
        super().update()

        # taylor expansion points
        t_q_re = self.node.taylor[util.TAC_V_RE][self.node.x, self.node.net.node_id]
        t_q_im = self.node.taylor[util.TAC_V_IM][self.node.x, self.node.net.node_id]

        # node admittance
        g_qq = self.node.net.gq[self.node.net.node_id]
        b_qq = self.node.net.bq[self.node.net.node_id]

        a = self.node.taylor[util.TAC_V_RE][self.node.x, :] @ self.node.net.g2q
        a += -self.node.taylor[util.TAC_V_IM][self.node.x, :] @ self.node.net.b2q
        b = self.node.taylor[util.TAC_V_RE][self.node.x, :] @ self.node.net.b2q
        b += self.node.taylor[util.TAC_V_IM][self.node.x, :] @ self.node.net.g2q

        # real coefficients
        m1 = a + 2 * g_qq * t_q_re
        m2 = b + 2 * g_qq * t_q_im
        m3 = self.node.net.g2q * t_q_re + self.node.net.b2q * t_q_im
        m4 = -self.node.net.b2q * t_q_re + self.node.net.g2q * t_q_im
        m5 = -g_qq * (t_q_re ** 2 + t_q_im ** 2) - t_q_re * a - t_q_im * b

        # imaginary coefficients
        n1 = -b - 2 * b_qq * t_q_re
        n2 = a - 2 * b_qq * t_q_im
        n3 = -self.node.net.b2q * t_q_re + self.node.net.g2q * t_q_im
        n4 = -self.node.net.g2q * t_q_re - self.node.net.b2q * t_q_im
        n5 = b_qq * (t_q_re ** 2 + t_q_im ** 2) + t_q_re * b - t_q_im * a

        self.node.A_eq[self.rows[0]][self.node.z[util.ZAC_V_Q_RE][0].row] = m1
        self.node.A_eq[self.rows[0]][self.node.z[util.ZAC_V_Q_IM][0].row] = m2
        self.node.b_eq[self.rows[0]] = -m5

        self.node.A_eq[self.rows[1]][self.node.z[util.ZAC_V_Q_RE][0].row] = n1
        self.node.A_eq[self.rows[1]][self.node.z[util.ZAC_V_Q_IM][0].row] = n2
        self.node.b_eq[self.rows[1]] = -n5

        for j in range(self.node.net.n2q):
            i_id = self.node.z[util.ZAC_V_I_RE][j].node_id

            self.node.A_eq[self.rows[0]][self.node.z[util.ZAC_V_I_RE][j].row] = m3[i_id]
            self.node.A_eq[self.rows[0]][self.node.z[util.ZAC_V_I_IM][j].row] = m4[i_id]

            self.node.A_eq[self.rows[1]][self.node.z[util.ZAC_V_I_RE][j].row] = n3[i_id]
            self.node.A_eq[self.rows[1]][self.node.z[util.ZAC_V_I_IM][j].row] = n4[i_id]


# P balance between generation, demand and injections at this node (q) [green]
class DCNodePowerBalanceEq(Equality):

    def __init__(self, name, node: DcNode):
        super().__init__(name=name, node=node, num_rows=1)

        # Constant terms for this equality
        # Conservation of real power at this node
        self.node.b_eq[self.rows[0]] = self.node.net.load[0]  # P demanded at this node
        self.node.A_eq[self.rows[0]][self.node.z[util.ZDC_P_GEN_Q][0].row] = 1     # P_gen
        self.node.A_eq[self.rows[0]][self.node.z[util.ZDC_P_INJ_Q][0].row] = -1    # P_inj

        if self.node.net.net_type == util.NET_AC_DC:
            for state_var in self.node.z[util.ZDC_P_C_K]:
                self.node.A_eq[self.rows[0]][state_var.row] = -1  # P converter (flow to ac side)
            for state_var in self.node.z[util.ZDC_PL_C_K]:
                self.node.A_eq[self.rows[0]][state_var.row] = -1  # P losses

    def update(self):
        super().update()
        # no variable terms, nothing to do here


# KCL at this node (q) [red]
class DCNodeCurrentEq(Equality):

    def __init__(self, name, node: DcNode):
        super().__init__(name=name, node=node, num_rows=1)

        # Constant terms for this equality
        # This node's terms (q)
        self.node.A_eq[self.rows[0]][self.node.z[util.ZDC_I_Q][0].row] = -1
        self.node.A_eq[self.rows[0]][self.node.z[util.ZDC_V_Q][0].row] = self.node.net.gq[self.node.net.node_id]

        # node i terms
        for j in range(self.node.net.n2q):
            i_id = self.node.z[util.ZDC_V_I][j].node_id

            # [V_i] red, real current injection
            self.node.A_eq[self.rows[0]][self.node.z[util.ZDC_V_I][j].row] = self.node.net.gq[i_id]

    def update(self):
        super().update()
        # no variable terms, nothing to do here


# Branch current flow between this node (q) and neighbouring nodes (i) [blue]
class DCBranchCurrentEq(Equality):

    def __init__(self, name, node: DcNode):
        super().__init__(name=name, node=node, num_rows=1 * node.net.n2q)

        # Constant terms for this equality
        for j in range(self.node.net.n2q):
            i_id = self.node.z[util.ZDC_V_I][j].node_id

            # current flow from node q to i
            self.node.A_eq[self.rows[j]][self.node.z[util.ZDC_V_I][j].row] = self.node.net.gq[i_id]
            self.node.A_eq[self.rows[j]][self.node.z[util.ZDC_I_QI][j].row] = -1
            self.node.A_eq[self.rows[j]][self.node.z[util.ZDC_V_Q][0].row] = -self.node.net.gq[i_id]

    def update(self):
        super().update()
        # no variable terms, nothing to do here


# Branch power flow between this node (q) and neighbouring nodes (i) [orange]
class DCBranchPowerEq(Equality):

    def __init__(self, name, node: DcNode):
        super().__init__(name=name, node=node, num_rows=1 * node.net.n2q)

        # Constant terms for this equality
        for j in range(self.node.net.n2q):
            self.node.A_eq[self.rows[j]][self.node.z[util.ZDC_P_QI][j].row] = -1

    def update(self):
        super().update()

        # Variable terms for this equality
        for j in range(self.node.net.n2q):
            i_id = self.node.z[util.ZDC_V_I][j].node_id
            g_qi = self.node.net.gq[i_id]  # conductance between this node (q) and neighbouring node (i)

            # taylor expansion points
            t_i = self.node.taylor[util.TDC_V][self.node.x, i_id]
            t_q = self.node.taylor[util.TDC_V][self.node.x, self.node.net.node_id]

            # coefficients
            b = g_qi * t_i - 2 * g_qi * t_q
            c = g_qi * t_q
            d = g_qi * t_q * (t_q - t_i)

            # update equalities
            self.node.A_eq[self.rows[j]][self.node.z[util.ZDC_V_I][j].row] = c
            self.node.A_eq[self.rows[j]][self.node.z[util.ZDC_V_Q][0].row] = b
            self.node.b_eq[self.rows[j]] = -d


# Power injection equations for this node [purple]
class DCNodePowerEq(Equality):

    def __init__(self, name, node: DcNode):
        super().__init__(name=name, node=node, num_rows=1)

        # Constant terms for this equality
        self.node.A_eq[self.rows[0]][self.node.z[util.ZDC_P_INJ_Q][0].row] = -1

    def update(self):
        super().update()

        # taylor expansion points
        t_q = self.node.taylor[util.TDC_V][self.node.x, self.node.net.node_id]

        # node admittance
        g_qq = self.node.net.gq[self.node.net.node_id]

        # real coefficients
        a0 = -t_q * self.node.net.gq @ self.node.taylor[util.TDC_V][self.node.x, :]
        ai = t_q * self.node.net.g2q
        aq = self.node.net.gq @ self.node.taylor[util.TDC_V][self.node.x, :] + g_qq * t_q

        self.node.A_eq[self.rows[0]][self.node.z[util.ZDC_V_Q][0].row] = aq
        self.node.b_eq[self.rows[0]] = -a0

        for j in range(self.node.net.n2q):
            i_id = self.node.z[util.ZDC_V_I][j].node_id
            self.node.A_eq[self.rows[0]][self.node.z[util.ZDC_V_I][j].row] = ai[i_id]


# AC/DC Converter losses, which are accounted for on the DC side
class DCConverterLossesEq(Equality):

    def __init__(self, name, node: DcNode):
        super().__init__(name=name, node=node, num_rows=1 * len(node.net.converters))

        # Constant terms for this equality
        for j, _ in enumerate(self.node.net.converters):
            self.node.A_eq[self.rows[j]][self.node.z[util.ZDC_PL_C_K][j].row] = -1

    def update(self):
        super().update()

        # Variable terms for this equality
        for j, converter in enumerate(self.node.net.converters):
            # taylor expansion points
            t_p = self.node.taylor[util.TDC_P_C_K][self.node.x, converter.converter_id]
            t_q = self.node.taylor[util.TDC_Q_C_K][self.node.x, converter.converter_id]

            # coefficients
            l1 = 2 * converter.loss_a * t_p / converter.s_max
            l2 = 2 * converter.loss_a * t_q / converter.s_max
            l3 = - converter.loss_a / converter.s_max * (t_p ** 2 + t_q ** 2) + converter.loss_b * converter.s_max

            # update equalities
            self.node.A_eq[self.rows[j]][self.node.z[util.ZDC_P_C_K][j].row] = l1
            self.node.A_eq[self.rows[j]][self.node.z[util.ZDC_Q_C_K][j].row] = l2
            self.node.b_eq[self.rows[j]] = -l3
