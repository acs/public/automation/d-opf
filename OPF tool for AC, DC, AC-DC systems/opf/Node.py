import numpy as np
from mpi4py import MPI
from cvxopt import matrix, solvers
from . import NetworkDef
from . import StateVariable
from . import util
from timing import Timer


class Node:

    def __init__(self, integrate_res, net: NetworkDef, comm):
        self.integrate_res = integrate_res          # True / 1 to integrate RES, ESS, CL components TODO
        self.net = net                              # system description instance, e.g. G, B matrix, limits, etc.
        self.comm = comm                            # MPI communication handle
        self.t = 0                                  # external iteration counter
        self.x = 0                                  # internal iteration counter
        self.z = {}                                 # state vector
        self.num_state_vars = 0                     # number of state vars  (set by build_state_vector)
        self.req_iter_x = np.zeros(self.net.max_iter_t + 1)     # Required number of internal iter. at each step
        self.taylor = {}                            # taylor operating points
        self.taylor_map = {}                        # map of taylor vars to state vars
        self.net_var_convergence_sum = []           # sum of global nodes that have reached convergence
        self.timers = {
            'computation': Timer(self.net.max_iter_t),
            'communication': Timer(self.net.max_iter_t),
            'total': Timer(self.net.max_iter_t)
        }
        self.comp_time = np.zeros(self.net.max_iter_t + 1)      # computation time at each iteration
        self.comm_time = np.zeros(self.net.max_iter_t + 1)      # communication time at each iteration

        self.init_state_vector()  # links indexes of nodes within above vectors
        self.init_taylor_points()  # build taylor point dictionary

        # Local solution containers with net structure (real, imaginary quantities)
        self.local_sol_v = np.zeros((self.net.max_iter_t + 1, self.net.num_nodes, 2))
        self.local_sol_gen = np.zeros((self.net.max_iter_t + 1, 2))

        # local solution, containing state vars, objective function value, flag
        self.local_iter_sol = np.zeros((self.net.max_iter_x, self.num_state_vars + 2))      # iterations in x
        self.local_sol = np.zeros((self.net.max_iter_t + 1, self.num_state_vars + 2))       # iterations in t

        # Dual variables
        self.y_v = np.zeros((self.net.max_iter_t + 1, self.net.num_nodes, 2))  # node voltage (re, im)

        # Net variables (average of local solutions)
        self.ave_v = np.zeros((self.net.max_iter_t + 1, self.net.num_nodes, 2))  # node voltage (re, im)

        # Communication matrix
        self.v_comm_mxq = np.tile(self.net.e2q, (self.net.max_iter_t + 1, 1))

        # Hybrid AC/DC network parameters
        if self.net.net_type == util.NET_AC_DC:
            # vars are index P, Q, P_losses
            self.local_sol_converter = np.zeros((self.net.max_iter_t + 1, self.net.global_converter_count, 3))
            self.ave_p = np.zeros((self.net.max_iter_t + 1, self.net.global_converter_count, 3))  # converter net var
            self.y_p = np.zeros((self.net.max_iter_t + 1, self.net.global_converter_count, 3))    # converter dual var
            self.p_comm_mxq = np.tile(self.net.aq, (self.net.max_iter_t + 1, 1))  # communication matrix

        # Objective function matrix and vector (z.T * H * z + f.T * z, where z is the state vector)
        self.H = np.zeros((self.num_state_vars, self.num_state_vars), dtype=np.double)
        self.f = np.zeros((self.num_state_vars,), dtype=np.double)

        # Optimization problem matrices converted to np array's after initialization
        self.A_ineq = []  # inequality constraints
        self.b_ineq = []  # inequality constraints
        self.inequalities = []  # inequality constraints
        self.A_eq = []  # equality constraints
        self.b_eq = []  # equality constraints
        self.equalities = []  # equality constraints

        # Set any constant values that won't vary iteration to iteration
        self.init_equalities()
        self.init_inequalities()
        self.init_optimization_matrices()

        # Initialize flat start (V_re = 1)
        self.ave_v[0, :, 0] = 1

    def init_state_vector(self):
        pass  # child class implementation

    def init_equalities(self):
        pass  # child class implementation

    def init_inequalities(self):
        pass  # child class implementation

    def init_optimization_matrices(self):
        pass  # child class implementation

    def init_taylor_points(self):
        # x + 1 iterations as the taylor points are used as the stopping criterion
        # state vars that have the same taylor var, will have the same taylor size
        for taylor_var, state_var in self.taylor_map.items():
            if len(self.z[state_var[0]]) > 0:
                self.taylor[taylor_var] = np.zeros((self.net.max_iter_x + 1, self.z[state_var[0]][0].taylor_size))
            else:
                self.taylor[taylor_var] = None

    def update_timing_iteration(self):
        for timer in self.timers.values():
            timer.t = self.t

    def add_neighbour_state_vars(self, neighbour_vars, row=0):
        for state_name, taylor_name in neighbour_vars.items():
            self.z[state_name] = [StateVariable.StateVariable(
                name=state_name,
                node_id=self.net.n2q_ids[i],
                row=row + i,
                taylor=taylor_name,
                taylor_size=self.net.num_nodes
            ) for i in range(self.net.n2q)]
            row += self.net.n2q

            # build taylor map
            if taylor_name not in self.taylor_map:
                self.taylor_map[taylor_name] = [state_name]
            else:
                self.taylor_map[taylor_name].append(state_name)
        return row

    def add_converter_state_vars(self, converter_vars, row=0):
        for state_name, taylor_name in converter_vars.items():
            self.z[state_name] = [StateVariable.StateVariable(
                name=state_name,
                node_id=converter.converter_id,
                row=row + i,
                taylor=taylor_name,
                taylor_size=self.net.global_converter_count
            ) for i, converter in enumerate(self.net.converters)]
            row += len(self.net.converters)

            # build taylor map
            if taylor_name not in self.taylor_map:
                self.taylor_map[taylor_name] = [state_name]
            else:
                self.taylor_map[taylor_name].append(state_name)
        return row

    def add_node_state_vars(self, node_vars, row=0):
        for state_name, taylor_name in node_vars.items():
            self.z[state_name] = [StateVariable.StateVariable(
                name=state_name,
                node_id=self.net.node_id,
                row=row,
                taylor=taylor_name,
                taylor_size=self.net.num_nodes
            )]
            row += 1

            # build taylor map
            if taylor_name not in self.taylor_map:
                self.taylor_map[taylor_name] = [state_name]
            else:
                self.taylor_map[taylor_name].append(state_name)

        return row

    def update_taylor_points(self):
        if self.x == 0:
            self.init_taylor_points()  # reset
            # build taylor point vector from previous local solution iteration (t)
            source_vec = self.local_sol[self.t, :]
        else:
            # build taylor point vector from previous local iteration (x)
            source_vec = self.local_iter_sol[self.x - 1, :]

        for taylor_var, state_vars in self.taylor_map.items():
            for state_var in state_vars:
                for var in self.z[state_var]:
                    self.taylor[taylor_var][self.x, var.node_id] = source_vec[var.row]

    def update_equalities(self):
        for equality in self.equalities:
            equality.update()

    def update_inequalities(self):
        for inequality in self.inequalities:
            inequality.update()

    def update_optimization_matrices(self):
        pass    # implement in child class

    def update_clone_variables(self):

        self.timers["computation"].start()
        self.x = 0
        self.local_iter_sol = np.zeros_like(self.local_iter_sol)
        while self.x < self.net.max_iter_x:
            self.update_taylor_points()
            if self.check_local_convergence():
                self.req_iter_x[self.t] = self.x
                break
            else:
                self.update_equalities()
                self.update_inequalities()
                self.update_optimization_matrices()

                solvers.options['show_progress'] = False  # default True
                solvers.options['maxiters'] = 100  # default 100
                solvers.options['abstol'] = 1e-7  # default 1e-7
                solvers.options['reltol'] = 1e-6  # default 1e-6
                solvers.options['feastol'] = 1e-7  # default 1e-7

                P = matrix(self.H)
                q = matrix(self.f)
                G = matrix(self.A_ineq)
                h = matrix(self.b_ineq)
                A = matrix(self.A_eq)
                b = matrix(self.b_eq)
                sol = solvers.qp(P, q, G, h, A, b)

                if sol['status'] == 'optimal':  # the problem is feasible and terminated without error
                    self.local_iter_sol[self.x][0:-2] = np.asarray(sol['x'])[:, 0]
                    self.local_iter_sol[self.x][-2] = sol['primal objective']
                    self.local_iter_sol[self.x][-1] = 1
                else:  # the problem is infeasible
                    util.log("unfeasible problem on node {}".format(self.net.node_id))
                    if self.x > 0:  # use previous solution
                        self.local_iter_sol[self.x] = self.local_iter_sol[self.x - 1]
                        self.local_iter_sol[self.x][-1] = 9991  # flag unfeasible problem
                    elif self.x == 0 and self.t > 0:
                        # we are in the first local iteration but not in the first global iteration
                        # use the previous solution
                        self.local_iter_sol[self.x] = self.local_sol[self.t]
                        self.local_iter_sol[self.x][-1] = 9992
                    elif self.x == 0 and self.t == 0:
                        # we are in the first internal & external iteration, non-feasible problem
                        raise RuntimeError("Initial solution could not be found for node ", self.net.node_id)

                self.x += 1

        # in case of no local convergence, the taylor points need to be updated once more
        # so that other methods can use the last values
        if self.x == self.net.max_iter_x:
            self.update_taylor_points()
            self.req_iter_x[self.t] = self.x

        # Results are 1-index (so as to preserve the starting conditions)
        # save local solution
        self.t += 1
        self.local_sol[self.t] = self.local_iter_sol[self.x - 1, :]
        self.timers["computation"].stop()

    def check_local_convergence(self):
        pass    # implement in child class

    def update_net_variables(self):
        self.update_net_voltages()

        if self.net.net_type == util.NET_AC_DC:
            self.update_net_converter_power()

    def update_dual_variables(self):
        delta_v = self.local_sol_v[self.t] - self.ave_v[self.t]
        self.y_v[self.t] = self.y_v[self.t - 1] + self.net.rho_v * delta_v

        if self.net.net_type == util.NET_AC_DC:
            delta_p = self.local_sol_converter[self.t] - self.ave_p[self.t]
            self.y_p[self.t] = self.y_p[self.t - 1] + self.net.rho_p * delta_p

    def update_net_voltages(self):
        # 1. Send / receive clone voltages
        received_voltages = self.send_receive_variables(
            send_values=self.local_sol_v[self.t],
            destinations=np.where(self.v_comm_mxq[self.t, :] == 1)[0],
            dest_offset=self.net.global_id_offset,
            tag=1
        )
        received_voltages[self.net.node_id] = self.local_sol_v[self.t, self.net.node_id]  # add own clone voltage

        # 2. compute net variables
        # Calculate the average voltage that all nodes have calculated for this node
        self.ave_v[self.t, self.net.node_id] = np.sum(received_voltages, axis=0) \
            / (np.sum(self.v_comm_mxq[self.t, :]) + 1)

        # 3. Send / receive net voltages
        send_ave = np.tile(self.ave_v[self.t, self.net.node_id, :], (self.net.num_nodes, 1))
        received_ave = self.send_receive_variables(
            send_values=send_ave,
            destinations=np.where(self.v_comm_mxq[self.t, :] == 1)[0],
            dest_offset=self.net.global_id_offset,
            tag=2
        )

        # Store net variables
        self.ave_v[self.t, self.net.n2q_ids] = received_ave[self.net.n2q_ids]

    def update_net_converter_power(self):
        if len(self.net.converters) == 0:
            return  # nothing to transmit

        # 1. Send / receive clone converter powers
        req = list()
        tag = 3
        received_converter_powers = np.zeros((self.net.global_converter_count, 2, 2))
        self.timers["communication"].start()
        for converter in self.net.converters:
            if self.p_comm_mxq[self.t, converter.converter_id] == 1:
                # 1. send value
                req.append(self.comm.Isend(
                    self.local_sol_converter[self.t, converter.converter_id, 0:2],
                    dest=converter.connecting_node_g_id,
                    tag=tag
                ))  # non-blocking

                # 2. receive values
                req.append(self.comm.Irecv(
                    received_converter_powers[converter.converter_id, 1, :],
                    source=converter.connecting_node_g_id,
                    tag=tag
                ))  # non-blocking

        MPI.Request.Waitall(req)
        self.timers["communication"].stop()
        received_converter_powers[:, 0, :] = self.local_sol_converter[self.t, :, 0:2]  # add own clone voltage

        # 2. compute net variables
        # Since there are only two end points to a converter, only one exchange of data is necessary
        self.ave_p[self.t, :, 0:2] = np.sum(received_converter_powers, axis=1) \
            / (self.p_comm_mxq[self.t, :, np.newaxis] + 1)

    def check_global_convergence(self):
        net_var_converged = True

        # 1. Check net variable convergence
        #   a. check voltage convergence
        delta_v = self.local_sol_v[self.t - 1] - self.ave_v[self.t]
        if np.absolute(delta_v).max() > self.net.v_conv:
            net_var_converged = False

        if self.net.net_type == util.NET_AC_DC:
            delta_p = self.local_sol_converter[self.t - 1, :, 0:2] - self.ave_p[self.t, :, 0:2]
            if np.absolute(delta_p).max() > self.net.p_conv:
                net_var_converged = False

        # store local convergence
        self.net_var_convergence_sum.append(self.send_receive_flags(flag=int(net_var_converged)))
        if self.net.global_id == self.net.ruler:
            util.log("Net var convergence flag sum = {}".format(self.net_var_convergence_sum[-1]))

        # 2. Verify Pgen is within reasonable limits if voltage convergence is met
        is_valid_operating_point = False
        if self.net_var_convergence_sum[-1] == self.net.global_node_count:
            # Gathering power generated in the ruler
            send_p_gen = np.array([self.local_sol_gen[self.t, 0]], dtype=float)
            received_p_gen = np.zeros((self.net.global_node_count,), dtype=float)
            self.comm.Gather(send_p_gen, received_p_gen, root=self.net.ruler)

            # Checking power in the ruler
            if self.net.global_id == self.net.ruler:
                is_valid_operating_point = self.net.is_power_generation_valid(np.sum(received_p_gen))

        # 3. Broadcast global convergence (from ruler)
        convergence_achieved = np.array([is_valid_operating_point], dtype=int)
        self.comm.Bcast(convergence_achieved, root=self.net.ruler)

        return convergence_achieved[0] == 1

    # send_values must be numpy array (num_nodes, num_vars)
    def send_receive_variables(self, send_values, destinations, dest_offset, tag):
        req = list()
        received_values = np.zeros((self.net.num_nodes, send_values.shape[1]))
        self.timers["communication"].start()
        for destination in destinations:
            # 1. send value
            req.append(self.comm.Isend(
                send_values[destination, :],
                dest=destination + dest_offset,
                tag=tag
            ))  # non-blocking

            # 2. receive values
            req.append(self.comm.Irecv(
                received_values[destination],
                source=destination + dest_offset,
                tag=tag
            ))  # non-blocking
        MPI.Request.Waitall(req)
        self.timers["communication"].stop()

        return received_values

    def send_receive_flags(self, flag):
        # Gathering the flags in the ruler entity
        send_flag = np.array([flag], dtype=int)
        received_flags = np.zeros((self.net.global_node_count,), dtype=int)
        self.timers["communication"].start()
        self.comm.Gather(send_flag, received_flags, root=self.net.ruler)
        self.timers["communication"].stop()
        flag_sum = np.sum(received_flags)

        # Broadcast the sum of flags fro the ruler to all the others
        broadcast_flag = np.array([flag_sum], dtype=int)
        self.timers["communication"].start()
        self.comm.Bcast(broadcast_flag, root=self.net.ruler)
        self.timers["communication"].stop()

        return broadcast_flag[0]
