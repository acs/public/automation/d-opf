import numpy as np
import time


class TimerError(Exception):
    """ custom exception """


class Timer:

    def __init__(self, max_iter):
        self.t = 0
        self.time_elapsed = np.zeros(max_iter)
        self._start_time = None

    def start(self) -> None:
        if self._start_time is not None:
            raise TimerError("Timer is running. Use .stop() to stop it")

        self._start_time = time.perf_counter()

    def stop(self) -> None:
        if self._start_time is None:
            raise TimerError("Timer is not running. Use .start() to start it")

        # Calculate and store elapsed time
        self.time_elapsed[self.t] += time.perf_counter() - self._start_time
        self._start_time = None

    def total(self) -> float:
        return np.sum(self.time_elapsed)

    def std(self) -> float:
        return np.std(self.time_elapsed[0:self.t])

    def avg(self) -> float:
        return np.average(self.time_elapsed[0:self.t])

    def min(self) -> float:
        return np.min(self.time_elapsed[0:self.t])

    def max(self) -> float:
        return np.max(self.time_elapsed[0:self.t])