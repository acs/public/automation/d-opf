# acs_hybrid

Welcome to the ACS distributed optimal power flow (DOPF) solver.

## Getting started

1. Make sure you have python, matlab and mpi installed.
2. Install the required python modules listed in `requirements.txt`.
3. Run `start.py` and follow the prompts.

## Input data structure
The data for each network structure should reside within its own subfolder in the `networks` folder.  This subfolder 
will consist of the following files:
#### config.json 
  ```
  {
      "net_type":"ac_dc_network",   # or 'ac_network' or 'dc_network'
      "global_node_count": 28,      # must equal total number of nodes
      "rho_v": 1e6,                 # ADMM penalty param for voltages
      "rho_p": 1e6,                 # ADMM penalty param for converter power (only for hybrid)
      "max_iter_t": 2e3,            # max global iterations
      "max_iter_x": 20,             # max local iterations
      "v_conv": 9e-5,               # voltage convergence criterion (global)
      "t_conv": 9e-7,               # taylor point convergence (local)
      "p_conv": 9e-5,               # converter power convergence (global)
      "dc": {                       # dc network vars (dc or hybrid only)
        "node_count": 14,               # dc note count 
        "data": "dc_data.xlsx",         # dc network data
        "default_max_p": 5,             # default max power flow constraint
        "default_max_i": 5              # default max current flow constraint
      },
      "ac": {                       # ac network vars (ac or hybrid only)
        "node_count": 14,               # ac note count 
        "data": "ac_data.xlsx",         # ac network data
        "default_max_p": 5,             # default max power flow constraint
        "default_max_i": 5              # default max current flow constraint
      },
      "converter": {                # converter data (hybrid only)
        "data": "converter_data.xlsx"
      }
  }
  ```
#### ac_data.xlsx 
(ac or hybrid networks only)

Generate this file using `create_ac_network_data.m`. Note you'll need `Matpower` installed to do so. Input parameters 
are `network_name` and `matpower_case_file_name`  
    
#### dc_data.xlsx
(dc or hybrid networks only)

Generate this file using `create_dc_network_data.m`. Note you'll need `Matpower` installed to do so. Input parameters 
are `{network_name}` and `{matpower_case_file_name}`.

Data is generated from the ac case file, by ignoring the imaginary part of Admittance parameters. Where no real
part is provided for branch resistances, the system's average X/R ratio is used to compute the resistance. 
      
#### converter_data.xlsx 
(hybrid networks only)

An `xlsx` file, with one sheet named `converter`, which contains the following columns:

1. `C_K`        Converter id (1, 2, 3...)
2. `AC_BUS`     id of connecting ac node (1-based index)
3. `DC_BUS`     id of connecting dc node (1-based index)
4. `S_MAX`      max converter power (pu)
5. `LOSS_A`     *a* coefficient of the converter loss formulation
6. `LOSS_B`     *b* coefficient of the converter loss formulation
  
## Code structure

#### Start.py
`Start.py` is the main entry point. It will load the networks in the `networks` folder and then prompt the user for 
various inputs. Finally, it will initiate the required number of processes via the `mpiexec` command. Each process will
launch `main.py`. In terms of id ordering, AC nodes come first.

#### Main.py
`Main.py` will load the relevant network data for the process (node) via the creation of a `NetworkDefinition` object.
It will then initialize the `Node` object and step through the algorithm until convergence (or the iteration limit) is 
reached.  Then the results will be exported in the `results.xlsx` file in the `results/{network_name}` subfolder. 
Lastly Matlab will be called to plot the results.

#### Node.py
The `Node` class and its two child classes, `AcNode` and `DcNode`, when combined with the `Equality` and `Inequality`
classes contain the required logic to execute the algorithm.  The state variables `z` are abstracted so that 
indices are not hard coded. Each state variable can contain one or more clone variables, for example all the voltages
of the neighbouring nodes. Additionally, each state variable can be mapped to a taylor variable.  Using this map, 
the taylor variables can be automatically created and updated. 

In general, the solution sequence is as follows:

1. init 
   - variables 
   - state variables
   - taylor variables
   - equalities 
   - inequalities
   - optimization matrices
   - set initial conditions (V = 1 + j0)

while t < max_iter_t and not_globally_converged    
2. `update_clone_variables`
    - x = 0
    - while x < max_iter_x
        - update taylor points
            - if x = 0, retrieve values from previous solution
        - check for convergence
            - if converged, exit x loop
        - update 
           - equalities 
           - inequalities
           - optimization matrices
        - solve quadratic optimization problem and save results
        - x += 1
    - store final results
    - t += 1
3. `update_net_variables`
    - Send/receive clone voltages
    - Compute own net voltage
    - Send/receive net voltages
    - Repeat above steps for converter powers (if hybrid network)
4.  `update_dual_variables`    
    - Compute dual variables based on net and clone voltages
5.  `check_global_convergence` 
    - Ruler gathers if every node has voltage (and converter power) convergence
        - If yes, ruler gathers generated power and checks if it is within [1, 1.3] times the load. 
            - If power generation is valid, exit t loop
    
6.  Save results and plot graphs 

## Results
Results will be saved in the subfolder `results/{network_name}`.  In general, rows represent iterations in `t` and 
columns represent `{node_id}` where AC nodes come before DC nodes. AC voltages are stored in polar form. Dash lines in
plots indicate summed values. 

