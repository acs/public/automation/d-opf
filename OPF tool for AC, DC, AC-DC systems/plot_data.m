

disp('Importing data to Matlab...')
txt=fopen('temp_identifier.txt');
identifier=fscanf(txt, '%s');
fclose(txt);
txt=fopen('temp_dirName.txt');
dirName=fscanf(txt, '%s');
fclose(txt);
currentfolder=pwd;
prefix = currentfolder  + "/" + dirName + "/" + identifier;

data_file = prefix + 'results.xlsx';
sheet_names = sheetnames(data_file);

if ismember('ac_v_mag', sheet_names)
    % There is AC data, so plot it
    plot_detail('AC Voltage', data_file, ["ac_v_mag"; "ac_v_ang"], ...
        ["voltage (p.u.)"; "ph. ang (deg)"], ...
        0.001, prefix, [false; false]);
    
    plot_detail('AC Power', data_file, ...
        ["ac_p_gen", "ac_p_load"; "ac_q_gen", "ac_q_load"], ...
        ["P (p.u.)", "Q (p.u.)"], 0.2, prefix, [true false; true false]);
    
    plot_summary('AC Power Summary', data_file, ["ac_p_gen"; "ac_q_gen"], ...
        ["AC Node", "AC Node"], ...
        ["P (p.u.)", "Q (p.u.)"], ...
        0.2, prefix);
end

if ismember('dc_v', sheet_names)
    % There is DC data, so plot it
    plot_detail('DC Voltage', data_file, [["dc_v"]], ...
        ["voltage (p.u.)"], ...
        0.001, prefix, [[false]]);
    
    plot_detail('DC Power', data_file, ...
        ["dc_p_gen", "dc_p_load"], ...
        ["P (p.u.)"], 0.2, prefix, [true false]);
    
    plot_summary('DC Power Summary', data_file, ["dc_p_gen"], ...
        ["DC Node", "DC Node"], ...
        ["P (p.u.)", "Q (p.u.)"], ...
        0.2, prefix);
end

if ismember('conv_p', sheet_names)
    % There is Converter data, so plot it
    plot_detail('Converter Power', data_file, ...
        ["conv_p"; "conv_q"; "conv_p_loss"], ...
        ["P (p.u.)", "Q (p.u.)", "P_{loss} (p.u.)"], 0.2, prefix, [true; true; true]);
    
    plot_summary('Converter Power Summary', data_file, ["conv_p"; "conv_q"; "conv_p_loss"], ...
        ["Converter", "Converter", "Converter"], ...
        ["P (p.u.)", "Q (p.u.)", "P_{loss} (p.u.)"], ...
        0.2, prefix);
end

plot_detail('Convergence', data_file, ...
    [["convergence"]], ...
    ["Nodes with convergence"], 1, prefix, [[false]]);


% Delete temp files
delete temp_*
disp('Saved. Exiting Matlab...')
exit

function [] = plot_detail(name, data_file, sheet_names, ...
    y_axis_labels, padding, prefix, cumulative)

    f = figure('Name', name);
    f.Position = [100 100 650 650];
    [num_plots, num_series] = size(sheet_names);
    sgtitle(name);
    
    line_types = ["-", "--", ":", "-."];
    
    for i = 1:num_plots
        subplot(num_plots, 1, i);
        hold on
        a = 0;
        b = 0;
        c = 0;
        
        for j = 1:num_series
            M = readmatrix(data_file, 'Sheet', sheet_names(i, j));
            M = M(2:end, 2:end);
            plot(M, line_types(j));

            % plot limits
            if j == 1
                [c, ~] = size(M);   % constant for all data in set
                a = min(M(:));
                b = max(M(:));
            else
                a = min(a, min(M(:)));
                b = max(b, max(M(:)));
            end
            
            % plot cumulative function
            if cumulative(i, j)
                N = sum(M, 2);
                plot(N, line_types(4));
                a = min(a, min(N(:)));
                b = max(b, max(N(:)));
            end
            
        end
        xlim([0 c]);
        ylim([(a - padding) (b + padding)]);
        xlabel('iteration number');
        ylabel(y_axis_labels(i));
        hold off
    end

    saveas(figure(1), gen_file_name(prefix + name, '.fig'));
    saveas(figure(1), gen_file_name(prefix + name, '.png'));
    close all
end

function [] = plot_summary(name, data_file, sheet_names, ...
    x_axis_labels, y_axis_labels, padding, prefix)

    f = figure('Name', name);
    f.Position = [100 100 650 650];
    num_plots = length(sheet_names);
    sgtitle(name);
    
    for i = 1:num_plots
        subplot(num_plots, 1, i);
        M = readmatrix(data_file, 'Sheet', sheet_names(i));
        M = M(end, 2:end);
        a = min(M(:));
        b = max(M(:));
        
        bar(M);
        text(1:length(M), M, compose('%.2f',M), ...
            'vert', 'bottom', 'horiz', 'center'); 
        box off

        ylim([(a - padding) (b + padding)]);
        xlabel(x_axis_labels(i));
        ylabel(y_axis_labels(i));
    end

    saveas(figure(1), gen_file_name(prefix + name, '.fig'));
    saveas(figure(1), gen_file_name(prefix + name, '.png'));
    close all
end

function file_name = gen_file_name(name, ext)
    file_name = lower(replace(name, " ", "_")) + ext;
end