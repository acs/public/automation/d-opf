% Generates dc network data from Matpower for use 
% with the Hybrid OPF solver. You'll find the data
% in the network folder under you choosen name.
%
% Ensure that Matpower is installed and the Matpower 
% startup script has been run. 

function result = create_dc_network_data(name, case_file)

    mpc = loadcase(case_file);
    dir = "networks/" + name;
    file_name = dir + "/dc_data.xlsx";
    if not(isfolder(dir))
        mkdir(dir);
    end
    
    if isfile(file_name)
        prompt = 'Do you want to overwrite the existing file? y/n [y]: ';
        str = input(prompt,'s');
        if str ~= 'y'
            result = false;
            return
        end
        delete(file_name);
    end
    
    export_bus_data(file_name, mpc);
    export_branch_data(file_name, mpc);
    export_gen_data(file_name, mpc);
    
    result = true;
end

function export_bus_data(file_name, mpc)

    header = [...
        "BUS_I", ...    % Bus number
        "PD", ...       % active power demand (pu)
        "GS", ...       % shunt conductance (pu)
        "VMAX", ...     % Max bus voltage (pu)
        "VMIN" ...      % Min bus voltage (pu)
    ];

    writematrix(header, file_name, 'Sheet', 'bus','WriteMode','append');
    
    % convert non-pu data to pu data
    % see Table B-1 in Matpower documentation for original units
    bus_data = mpc.bus(:,[1, 3, 5, 12:13]);
    bus_data(:, 2:3) = bus_data(:, 2:3) / mpc.baseMVA;
    
    writematrix(bus_data, file_name, 'Sheet', 'bus','WriteMode','append');

end

function export_branch_data(file_name, mpc)

    header = [...
        "F_BUS", ...    % From bus
        "T_BUS", ...    % To bus
        "BR_R", ...     % branch resistance (pu)
        "P_MAX", ...    % branch power flow limits (pu)
        "I_MAX", ...    % branch current flow limits (pu)
    ];

    writematrix(header, file_name, 'Sheet', 'branch','WriteMode','append');
    
    % compute average x/r ratio to estimate branch resistances
    % for lines without r given.
    x_r_ratio = mpc.branch(:,4) ./ mpc.branch(:,3);
    x_r_ratio = x_r_ratio(isfinite(x_r_ratio)); 
    avg_x_r_ratio = sum(x_r_ratio) / length(x_r_ratio);
    
    branch_data = mpc.branch(:, [1:3, 6, 6]);
    branch_data(:, 4:5) = branch_data(:, 4:5) / mpc.baseMVA;
    branch_data(branch_data(:,3)==0, 3) = ...
         mpc.branch(branch_data(:,3)==0, 4) ./ avg_x_r_ratio;
    
    writematrix(branch_data, file_name, 'Sheet', 'branch','WriteMode','append');

end

function export_gen_data(file_name, mpc)

    header = [...
        "GEN_BUS", ...  % Bus id
        "PMAX", ...     % max active power (pu)
        "PMIN", ...     % min active power (pu)
        "COST_A", ...   % quadratic cost coefficent (pu)
        "COST_B" ...    % linear cost coefficent (pu)
        "COST_C" ...    % constant cost coefficent (pu)
    ];

    writematrix(header, file_name, 'Sheet', 'gen','WriteMode','append');
    
    gen_data = mpc.gen(:,[1, 9:10]);
    gen_data(:, 2:3) = gen_data(:, 2:3) ./ mpc.gen(:, 7); % Mbase for gen
    
    gen_data(:, 4) = mpc.gencost(:, 5) .* mpc.gen(:, 7).^2;
    gen_data(:, 5) = mpc.gencost(:, 6) .* mpc.gen(:, 7);
    gen_data(:, 6) = mpc.gencost(:, 7);
    
    writematrix(gen_data, file_name, 'Sheet', 'gen','WriteMode','append');

end
