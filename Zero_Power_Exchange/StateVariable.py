

class StateVariable:

    def __init__(self, name, node_id, row, taylor=None, taylor_size=0):
        self.name = name
        self.node_id = node_id  # node id or converter id
        self.row = row
        self.taylor = taylor
        self.taylor_size = taylor_size
