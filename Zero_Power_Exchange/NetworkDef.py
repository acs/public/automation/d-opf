# imports
import math
import numpy as np
import pandas as pd
import json
import os
import cmath
from . import util
from . import Converter


# Loads the network definition data corresponding to this node
class NetworkDef:

    def __init__(self, config_file_path, global_id=-1, ruler=-1):
        self.config_file_path = config_file_path
        self.ruler = ruler  # node that will gather results and determine final convergence
        self.global_id = global_id  # global id of the node within the entire network

        with open(config_file_path + "/config.json") as f:
            config_data = json.load(f)
            self.net_type = config_data['net_type']  # ac, dc, ac/dc
            self.global_node_count = config_data['global_node_count']  # total node count in entire network

            # stop computing further information if global_id wasn't provided
            # start.py calls this script and only needs the global_node_count
            if global_id == -1:
                return
            self.global_bus_id = []
            # Algorithm Parameters
            self.rho_v = int(config_data['rho_v'])  # ADMM penalty parameter for voltage
            self.max_iter_t = int(config_data['max_iter_t'])  # max number of external iterations
            self.max_iter_x = int(config_data['max_iter_x'])  # max number of internal iterations
            self.v_conv = float(config_data['v_conv'])  # Convergence criterion for the voltage
            self.t_conv = float(config_data['t_conv'])  # Convergence criterion for the taylor points
            self.power_balance = float(config_data['power_balance'])  # Power balance criterion
            self.curtailment = float(config_data['flex_curtailment'])

            self.ac_node_count = 0
            self.dc_node_count = 0
            if 'ac' in config_data:
                self.ac_node_count = config_data['ac']['node_count']
            if 'dc' in config_data:
                self.dc_node_count = config_data['dc']['node_count']

            # Common AC / DC parameters
            self.costs = np.zeros(3)  # generation costs for this node
            self.costs_flex = np.zeros(3)  # generation costs for this node
            # Add cost flex name
            self.global_load = np.zeros(2)  # Total system load
            self.dc_load = np.zeros(2)  # load in dc system
            self.ac_load = np.zeros(2)  # load in ac system
            self.limits = {}  # System Limits

            # Warm start definition
            self.warm_V_real = []
            self.warm_V_im = []
            self.warm_P_gen = []
            self.warm_Q_gen = []


            self.total_internal_gen_p= np.zeros(1)
            self.total_internal_gen_q= np.zeros(1)

            self.total_external_gen_p = np.zeros(1)
            self.total_external_gen_q = np.zeros(1)

            self.total_max_flexible_load_p = np.zeros(1)
            self.total_max_flexible_load_q = np.zeros(1)



            # AC nodes come first, then DC in terms of global node id
            if self.is_ac_node(self.global_id):
                self.node_type = util.AC_NODE
                self.node_id = self.global_id  # AC net id (local id)
                self.global_id_offset = 0
                self.num_nodes = self.ac_node_count

                self.gq = np.zeros(self.num_nodes)  # row of G matrix for this node
                self.g2q = np.zeros(self.num_nodes)  # row of G matrix for this node (without own conductance)
                self.bq = np.zeros(self.num_nodes)  # row of B matrix for this node
                self.b2q = np.zeros(self.num_nodes)  # row of B matrix for this node (without own susceptance)
                self.eq = np.zeros(self.num_nodes)  # node's connection matrix
                self.e2q = np.zeros(self.num_nodes)  # node's connection matrix to neighbouring nodes
                self.load = np.zeros(2)  # Nodes' P, Q demand
                self.init_limits(config_data['ac'])
                self.load_ac_network_data(config_data['ac'])

                if self.net_type == util.NET_AC_DC:
                    self.load_other_network_load(config_data['dc'])  # complete global load, for convergence check

            else:
                self.node_type = util.DC_NODE
                self.global_id_offset = self.ac_node_count  # node id for entire network
                self.node_id = self.global_id - self.global_id_offset  # DC net id (local id)
                self.num_nodes = self.dc_node_count

                self.gq = np.zeros(self.num_nodes)  # row of G matrix for this node
                self.g2q = np.zeros(self.num_nodes)  # row of G matrix for this node (without own conductance)
                self.eq = np.zeros(self.num_nodes)  # node's connection matrix
                self.e2q = np.zeros(self.num_nodes)  # node's connection matrix to neighbouring nodes
                self.load = np.zeros(1)  # Nodes' P, Q demand
                self.init_limits(config_data['dc'])
                self.load_dc_network_data(config_data['dc'])

                if self.net_type == util.NET_AC_DC:
                    self.load_other_network_load(config_data['ac'])  # complete global load, for convergence check

            if self.net_type == util.NET_AC_DC:
                self.rho_p = int(config_data['rho_p'])  # ADMM penalty parameter for power
                self.p_conv = config_data['p_conv']  # convergence criterion for converter power
                self.aq = []  # converter incidence vector, converted to numpy array load_...
                self.converters = []  # converters connected to this node
                self.global_converter_count = 0  # number of converters in the entire network
                self.dc_nodes_with_converters = []  # DC node of the converters, to be used for data collection
                self.load_converter_data(config_data['converter'])

        self.nq = sum(self.eq)  # number of local nodes
        self.nq_ids = np.asarray(self.eq).nonzero()[0]  # ids of local nodes
        self.n2q = sum(self.e2q)  # number of neighbouring nodes
        self.n2q_ids = np.asarray(self.e2q).nonzero()[0]  # ids of neighbouring nodes

        self.validate_network_config()

    def validate_network_config(self):
        pass

    def is_power_generation_valid(self, p_gen, p_flex):  # Call also flexible load.
        #return (self.global_load[0] + p_flex < p_gen < self.power_balance * (self.global_load[0] + p_flex))
        return (self.global_load[0] + p_flex < p_gen) #for relaxing the power balance criterion, by avoiding specific acceptable percentage of losses

    def get_ac_global_id(self, ac_node_id):
        return ac_node_id

    def get_dc_global_id(self, dc_node_id):
        return dc_node_id + self.ac_node_count

    def is_ac_node(self, global_id):
        return global_id in range(0, self.ac_node_count)

    def is_dc_node(self, global_id):
        return not self.is_ac_node(global_id)

    def init_limits(self, config_data):
        self.limits = {
            util.L_V_MIN: np.zeros(self.num_nodes, float),
            util.L_V_MAX: np.zeros(self.num_nodes, float),
            util.L_I_FLOW_MAX: np.zeros(self.num_nodes, float),
            util.L_S_FLOW_MAX: np.zeros(self.num_nodes, float),  # ac
            util.L_P_FLOW_MAX: np.zeros(self.num_nodes, float),  # dc
            util.L_I_INJ_MAX: np.zeros(self.num_nodes, float),
            util.L_P_INJ_MAX: np.zeros(self.num_nodes, float),
            util.L_Q_INJ_MAX: np.zeros(self.num_nodes, float),  # ac
            util.L_S_INJ_MAX: np.zeros(self.num_nodes, float),  # ac

            util.L_P_GEN_MIN: np.zeros(self.num_nodes, float),
            util.L_Q_GEN_MIN: np.zeros(self.num_nodes, float),  # ac
            util.L_P_GEN_MAX: np.zeros(self.num_nodes, float),
            util.L_Q_GEN_MAX: np.zeros(self.num_nodes, float),  # ac
            util.L_S_GEN_MAX: np.zeros(self.num_nodes, float),  # ac

            util.L_P_FLEX_MIN: np.zeros(self.num_nodes, float),
            util.L_Q_FLEX_MIN: np.zeros(self.num_nodes, float),  # ac
            util.L_P_FLEX_MAX: np.zeros(self.num_nodes, float),
            util.L_Q_FLEX_MAX: np.zeros(self.num_nodes, float),  # ac
            util.L_S_FLEX_MAX: np.zeros(self.num_nodes, float),  # ac

            # Add 5 limit of flex load. //done
            util.L_P_CONV_MAX: np.zeros(self.num_nodes, float),
            util.L_Q_CONV_MAX: np.zeros(self.num_nodes, float),
            util.L_S_CONV_MAX: np.zeros(self.num_nodes, float)
        }

        self.limits[util.L_I_INJ_MAX][self.node_id] = config_data['default_max_i']
        self.limits[util.L_P_INJ_MAX][self.node_id] = config_data['default_max_p']
        self.limits[util.L_Q_INJ_MAX][self.node_id] = config_data['default_max_p']  # ac
        self.limits[util.L_S_INJ_MAX][self.node_id] = config_data['default_max_p']  # ac

    def load_ac_network_data(self, config_data):

        df_warm = pd.read_excel(io=os.path.join(self.config_file_path, config_data['warm']), header=0, index_col=None,
                                dtype=float)
        self.warm_V_real = df_warm["V_Real"]
        self.warm_V_im = df_warm["V_im"]
        self.warm_P_gen = df_warm["P_gen"]
        self.warm_Q_gen = df_warm["Q_gen"]

        df = pd.read_excel(
            io=os.path.join(self.config_file_path, config_data['data']),
            # sheet_name=['bus', 'branch', 'gen'],
            sheet_name=['bus', 'branch', 'gen', 'flex'],
            header=0,
            index_col=None,
            dtype=float
        )

        # bus data

        for row, bus in df['bus'].iterrows():
            bus_id = int(bus['BUS_I'] - 1)
            self.global_load += np.array([bus['PD'], bus['QD']])
            self.ac_load += np.array([bus['PD'], bus['QD']])
            self.limits[util.L_V_MIN][bus_id] = bus['VMIN']
            self.limits[util.L_V_MAX][bus_id] = bus['VMAX']

            if bus_id == self.node_id:
                self.load = np.array([bus['PD'], bus['QD']])
                # shunt elements, see equation 3.13 of Matpower documentation
                self.gq[self.node_id] += bus['GS']
                self.bq[self.node_id] += bus['BS']

        # branch data
        for row, branch in df['branch'].iterrows():
            from_node = int(branch['F_BUS'] - 1)
            to_node = int(branch['T_BUS'] - 1)

            # determine whether this node is at the to or from end
            if self.node_id == from_node:
                neighbour_id = to_node
            elif self.node_id == to_node:
                neighbour_id = from_node
            else:
                # branch not connected to this node
                continue

            # apply default limits if limits were not applied by Matpower
            if math.isclose(branch['S_MAX'], 0):
                self.limits[util.L_S_FLOW_MAX][neighbour_id] = config_data['default_max_p']
            else:
                self.limits[util.L_S_FLOW_MAX][neighbour_id] = branch['S_MAX']

            if math.isclose(branch['I_MAX'], 0):
                self.limits[util.L_I_FLOW_MAX][neighbour_id] = config_data['default_max_i']
            else:
                self.limits[util.L_I_FLOW_MAX][neighbour_id] = branch['I_MAX']

            # Compute branch contributions to the g and b vectors
            # see equation 3.2, 3.3, 3.11 - 3.13 of Matpower documentation
            y_s = 1 / complex(branch['BR_R'], branch['BR_X'])
            if branch['TAP'] == 0:
                tap = 1
            else:
                tap = branch['TAP']
            if self.node_id == from_node:
                y_qq = (y_s + 1j * branch['BR_B'] / 2) * 1 / tap ** 2
                y_qi = -y_s * 1 / (tap * cmath.exp(-1j * branch['SHIFT'] * cmath.pi / 180))
            else:
                y_qq = (y_s + 1j * branch['BR_B'] / 2)
                y_qi = -y_s * 1 / (tap * cmath.exp(1j * branch['SHIFT'] * cmath.pi / 180))

            self.gq[self.node_id] += y_qq.real
            self.bq[self.node_id] += y_qq.imag
            self.gq[neighbour_id] += y_qi.real
            self.bq[neighbour_id] += y_qi.imag

        # generator data
        for row, gen in df['gen'].iterrows():
            bus_id = int(gen['GEN_BUS'] - 1)
            self.global_bus_id.append(bus_id)

            if bus_id == self.node_id:  # A quadratic, B Linear.
                self.costs = np.array([gen['COST_A'], gen['COST_B'], gen['COST_C']])

                self.limits[util.L_P_GEN_MIN][bus_id] = gen['PMIN']
                self.limits[util.L_P_GEN_MAX][bus_id] = gen['PMAX']
                self.limits[util.L_Q_GEN_MIN][bus_id] = gen['QMIN']
                self.limits[util.L_Q_GEN_MAX][bus_id] = gen['QMAX']
                self.limits[util.L_S_GEN_MAX][bus_id] = abs(gen['PMAX'] + 1j * gen['QMAX'])

        # Flexible load
        for row, flex in df['flex'].iterrows():
            bus_id = int(flex['GEN_BUS'] - 1)

            if bus_id == self.node_id:  # A quadratic, B Linear.
                self.costs_flex = np.array([flex['COST_A'], flex['COST_B'], flex['COST_C']])

                self.limits[util.L_P_FLEX_MIN][bus_id] = self.curtailment * flex['PMAX'] #for curtailment factor equal to all flexible loads, as percentage of MAX power value, provided in config.json file
                #self.limits[util.L_P_FLEX_MIN][bus_id] = flex['PMIN'] #for MIN value of flexible load (curtailed load), individual for each flex load, provided in excel file with system data
                self.limits[util.L_P_FLEX_MAX][bus_id] = flex['PMAX']
                self.limits[util.L_Q_FLEX_MIN][bus_id] = self.curtailment * flex['QMAX']
                #self.limits[util.L_Q_FLEX_MIN][bus_id] = flex['QMIN']
                self.limits[util.L_Q_FLEX_MAX][bus_id] = flex['QMAX']
                self.limits[util.L_S_FLEX_MAX][bus_id] = abs(flex['PMAX'] + 1j * flex['QMAX'])


        # Cost dataframe
        df['gen'].index = df['gen']["GEN_BUS"] - 1
        df['gen'] = df['gen'].reindex(list(range(0, int(df['bus']["BUS_I"].max()))), fill_value=0)
        df_cost = df['gen'][['COST_A', 'COST_B', 'COST_C']].copy()
        self.costs_df = df_cost

        #Cost flexible
        df['flex'].index = df['flex']["GEN_BUS"] - 1
        df['flex'] = df['flex'].reindex(list(range(0, int(df['bus']["BUS_I"].max()))), fill_value=0)
        df_cost_flex = df['flex'][['COST_A', 'COST_B', 'COST_C']].copy()
        self.costs_df_flex = df_cost_flex

        # post-processing
        self.g2q = np.copy(self.gq)
        self.g2q[self.node_id] = 0

        self.b2q = np.copy(self.bq)
        self.b2q[self.node_id] = 0

        self.eq = np.logical_or(self.gq != 0, self.bq != 0) * 1  # This node's incidence matrix based on gq & bq
        self.e2q = np.logical_or(self.g2q != 0, self.b2q != 0) * 1

    def load_dc_network_data(self, config_data):

        df = pd.read_excel(
            io=os.path.join(self.config_file_path, config_data['data']),
            sheet_name=['bus', 'branch', 'gen'],
            header=0,
            index_col=None,
            dtype=float
        )

        # bus data
        for row, bus in df['bus'].iterrows():
            bus_id = int(bus['BUS_I'] - 1)
            self.global_load += np.array([bus['PD'], 0])
            self.dc_load += np.array([bus['PD'], 0])
            self.limits[util.L_V_MIN][bus_id] = bus['VMIN']
            self.limits[util.L_V_MAX][bus_id] = bus['VMAX']

            if bus_id == self.node_id:
                self.load = np.array([bus['PD'], 0])
                # shunt element, see equation 3.13 of Matpower documentation
                self.gq[self.node_id] += bus['GS']

        # branch data
        for row, branch in df['branch'].iterrows():
            from_node = int(branch['F_BUS'] - 1)
            to_node = int(branch['T_BUS'] - 1)

            # determine whether this node is at the to or from end
            if self.node_id == from_node:
                neighbour_id = to_node
            elif self.node_id == to_node:
                neighbour_id = from_node
            else:
                # branch not connected to this node
                continue

            # apply default limits if limits were not applied by Matpower
            if math.isclose(branch['P_MAX'], 0):
                self.limits[util.L_P_FLOW_MAX][neighbour_id] = config_data['default_max_p']
            else:
                self.limits[util.L_P_FLOW_MAX][neighbour_id] = branch['P_MAX']

            if math.isclose(branch['I_MAX'], 0):
                self.limits[util.L_I_FLOW_MAX][neighbour_id] = config_data['default_max_i']
            else:
                self.limits[util.L_I_FLOW_MAX][neighbour_id] = branch['I_MAX']

            # Compute branch contributions to the g and b vectors
            # see equation 3.2, 3.3, 3.11 - 3.13 of Matpower documentation
            y_s = 1 / branch['BR_R']
            self.gq[self.node_id] += y_s
            self.gq[neighbour_id] += -y_s

        # generator data
        for row, gen in df['gen'].iterrows():
            bus_id = int(gen['GEN_BUS'] - 1)

            if bus_id == self.node_id:
                self.costs = np.array([gen['COST_A'], gen['COST_B'], gen['COST_C']])
                self.limits[util.L_P_GEN_MIN][bus_id] = gen['PMIN']
                self.limits[util.L_P_GEN_MAX][bus_id] = gen['PMAX']

        # post-processing
        self.g2q = np.copy(self.gq)
        self.g2q[self.node_id] = 0

        self.eq = (self.gq != 0) * 1  # This node's incidence matrix based on gq
        self.e2q = (self.g2q != 0) * 1

    def load_converter_data(self, config_data):

        df = pd.read_excel(
            io=os.path.join(self.config_file_path, config_data['data']),
            sheet_name=['converter'],
            header=0,
            index_col=None,
            dtype=float
        )

        # converter data
        for row, converter in df['converter'].iterrows():
            converter_id = int(converter['C_K'] - 1)
            ac_bus_id = int(converter['AC_BUS'] - 1)
            dc_bus_id = int(converter['DC_BUS'] - 1)
            self.global_converter_count += 1
            self.dc_nodes_with_converters.append(dc_bus_id)
            self.aq.append(0)

            if row != converter_id:
                raise RuntimeError('Converter ids must start at 1 and increment by 1')

            if self.node_type == util.DC_NODE:
                if dc_bus_id == self.node_id:
                    self.aq[-1] = 1  # this converter is connected to this node
                    self.converters.append(Converter.Converter(
                        converter_id=converter_id,
                        node_id=self.node_id,  # dc local id
                        connecting_node_g_id=self.get_ac_global_id(ac_bus_id),  # ac global id
                        s_max=converter['S_MAX'],  # Converter's apparent power rating (pu)
                        loss_a=converter['LOSS_A'],  # loss coefficient A
                        loss_b=converter['LOSS_B']  # loss coefficient B
                    ))
            elif self.node_type == util.AC_NODE:
                if ac_bus_id == self.node_id:
                    self.aq[-1] = 1  # this converter is connected to this node
                    self.converters.append(Converter.Converter(
                        converter_id=converter_id,
                        node_id=self.node_id,  # ac local id
                        connecting_node_g_id=self.get_dc_global_id(dc_bus_id),  # dc global id
                        s_max=converter['S_MAX'],  # Converter's apparent power rating (pu)
                        loss_a=converter['LOSS_A'],  # loss coefficient A
                        loss_b=converter['LOSS_B']  # loss coefficient B
                    ))

        self.aq = np.asarray(self.aq)

    def load_other_network_load(self, config_data):

        df = pd.read_excel(
            io=os.path.join(self.config_file_path, config_data['data']),
            sheet_name=['bus'],
            header=0,
            index_col=None,
            dtype=float
        )

        # bus data
        for row, bus in df['bus'].iterrows():
            if self.node_type == util.DC_NODE:
                self.global_load += np.array([bus['PD'], bus['QD']])  # loading AC load
                self.ac_load += np.array([bus['PD'], bus['QD']])  # loading AC load
            else:
                self.global_load += np.array([bus['PD'], 0])  # loading DC load
                self.dc_load += np.array([bus['PD'], 0])  # loading DC load


if __name__ == "__main__":
    net = NetworkDef('../networks/hybrid_14ac_5dc/config.json', global_id=8)
