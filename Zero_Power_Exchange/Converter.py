

class Converter:

    def __init__(self, converter_id, node_id, connecting_node_g_id, s_max, loss_a, loss_b):
        self.converter_id = converter_id
        self.node_id = node_id      # ac or dc node id
        self.connecting_node_g_id = connecting_node_g_id    # global_node_id
        self.s_max = s_max
        self.loss_a = loss_a
        self.loss_b = loss_b
