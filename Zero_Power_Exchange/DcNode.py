# Imports
import numpy as np
from . import util
from . import Node
from . import NetworkDef
from . import Equality
from . import Inequality


class DcNode(Node.Node):

    def __init__(self, integrate_res, net: NetworkDef, comm):
        super().__init__(integrate_res=integrate_res, net=net, comm=comm)

    def init_state_vector(self):

        # state var id : taylor id
        neighbour_vars = {
            util.ZDC_V_I: util.TDC_V,     # Vi Neighbour clone voltages
            util.ZDC_I_QI: util.TDC_I,    # I_qi Neighbour branch currents
            util.ZDC_P_QI: util.TDC_P,    # P_qi Neighbour branch power
        }

        row = super().add_neighbour_state_vars(neighbour_vars, row=0)

        if self.net.net_type == util.NET_AC_DC:
            # state var id : taylor id
            converter_vars = {
                util.ZDC_P_C_K: util.TDC_P_C_K,    # Converter power sent to ac node
                util.ZDC_PL_C_K: util.TDC_PL_C_K,  # Converter power losses
                util.ZDC_Q_C_K: util.TDC_Q_C_K     # Converter reactive power injected in ac node
            }

            row = super().add_converter_state_vars(converter_vars, row=row)

        # state var id : taylor id
        node_vars = {
            util.ZDC_P_GEN_Q: util.TDC_GEN_P,     # Own power generation
            util.ZDC_P_INJ_Q: util.TDC_P,         # Own real power injected
            util.ZDC_I_Q: util.TDC_I,             # Own current injected
            util.ZDC_V_Q: util.TDC_V,             # Own clone voltage
        }

        # add and store number of state variables
        self.num_state_vars = super().add_node_state_vars(node_vars, row=row)

    def init_equalities(self):
        self.equalities.append(Equality.DCNodePowerBalanceEq(name='P_q', node=self))

        if self.net.net_type == util.NET_AC_DC:
            self.equalities.append(Equality.DCConverterLossesEq(name='Pl_C_k', node=self))

        self.equalities.append(Equality.DCNodeCurrentEq(name='I_q', node=self))
        self.equalities.append(Equality.DCBranchCurrentEq(name='I_qi', node=self))
        self.equalities.append(Equality.DCBranchPowerEq(name='P_qi', node=self))
        self.equalities.append(Equality.DCNodePowerEq(name='P_q_inj', node=self))

        # convert to numpy arrays now that all constraints are known
        self.A_eq = np.asarray(self.A_eq, dtype=np.double)
        self.b_eq = np.asarray(self.b_eq, dtype=np.double)

    def init_inequalities(self):

        # min
        self.inequalities.append(Inequality.MinConstraint(
            name='v_i_min', node=self, z=util.ZDC_V_I, mag_min=util.L_V_MIN))

        self.inequalities.append(Inequality.MinConstraint(
            name='i_qi_min', node=self, z=util.ZDC_I_QI, mag_min=util.L_I_FLOW_MAX, multiplier=-1))

        self.inequalities.append(Inequality.MinConstraint(
            name='p_qi_min', node=self, z=util.ZDC_P_QI, mag_min=util.L_P_FLOW_MAX, multiplier=-1))

        self.inequalities.append(Inequality.MinConstraint(
            name='p_gen_min', node=self, z=util.ZDC_P_GEN_Q, mag_min=util.L_P_GEN_MIN))

        self.inequalities.append(Inequality.MinConstraint(
            name='p_q_min', node=self, z=util.ZDC_P_INJ_Q, mag_min=util.L_P_INJ_MAX, multiplier=-1))

        self.inequalities.append(Inequality.MinConstraint(
            name='i_q_min', node=self, z=util.ZDC_I_Q, mag_min=util.L_I_INJ_MAX, multiplier=-1))

        self.inequalities.append(Inequality.MinConstraint(
            name='v_q_min', node=self, z=util.ZDC_V_Q, mag_min=util.L_V_MIN))

        # max
        self.inequalities.append(Inequality.MaxConstraint(
            name='v_i_max', node=self, z=util.ZDC_V_I, mag_max=util.L_V_MAX))

        self.inequalities.append(Inequality.MaxConstraint(
            name='i_qi_max', node=self, z=util.ZDC_I_QI, mag_max=util.L_I_FLOW_MAX))

        self.inequalities.append(Inequality.MaxConstraint(
            name='p_qi_max', node=self, z=util.ZDC_P_QI, mag_max=util.L_P_FLOW_MAX))

        self.inequalities.append(Inequality.MaxConstraint(
            name='p_gen_max', node=self, z=util.ZDC_P_GEN_Q, mag_max=util.L_P_GEN_MAX))

        if self.net.net_type == util.NET_AC_DC:
            for converter in self.net.converters:
                self.inequalities.append(Inequality.ConverterConvexMaxConstraint(
                    name='C_k_s_max', node=self, z_re=util.ZDC_P_C_K, z_im=util.ZDC_Q_C_K))

        self.inequalities.append(Inequality.MaxConstraint(
            name='p_q_max', node=self, z=util.ZDC_P_INJ_Q, mag_max=util.L_P_INJ_MAX))

        self.inequalities.append(Inequality.MaxConstraint(
            name='i_q_max', node=self, z=util.ZDC_I_Q, mag_max=util.L_I_INJ_MAX))

        self.inequalities.append(Inequality.MaxConstraint(
            name='v_q_max', node=self, z=util.ZDC_V_Q, mag_max=util.L_V_MAX))

        # convert to numpy arrays now that all constraints are known
        self.A_ineq = np.asarray(self.A_ineq, dtype=np.double)
        self.b_ineq = np.asarray(self.b_ineq, dtype=np.double)

    def init_optimization_matrices(self):
        # H Matrix, squared terms of the objective function (1/2 * z.T * H * z) (the whole matrix is constant)

        # penalty for clone variables vs. net variable
        def add_rho_terms(state_var, rho):
            for z in self.z[state_var]:
                self.H[z.row, z.row] = rho

        add_rho_terms(util.ZDC_V_I, self.net.rho_v)
        add_rho_terms(util.ZDC_V_Q, self.net.rho_v)

        if self.net.net_type == util.NET_AC_DC:
            add_rho_terms(util.ZDC_P_C_K, self.net.rho_p)
            add_rho_terms(util.ZDC_Q_C_K, self.net.rho_p)

        # Pgen costs ki2) for this node
        self.H[self.z[util.ZDC_P_GEN_Q][0].row, self.z[util.ZDC_P_GEN_Q][0].row] = 2*self.net.costs[0]

        # f vector, first-order terms of the objective function (f.T * z)
        # Pgen costs (ki1) for this node
        self.f[self.z[util.ZDC_P_GEN_Q][0].row] = self.net.costs[1]

    def update_optimization_matrices(self):
        # helper function to update voltage penalty term
        def update_voltage_penalty(state_var):
            for z in self.z[state_var]:
                self.f[z.row] = self.y_v[self.t, z.node_id, 0] \
                                - self.net.rho_v * self.ave_v[self.t, z.node_id, 0]

        update_voltage_penalty(util.ZDC_V_I)
        update_voltage_penalty(util.ZDC_V_Q)

        if self.net.net_type == util.NET_AC_DC and len(self.net.converters) > 0:
            # converter power penalty
            for i, converter in enumerate(self.net.converters):
                self.f[self.z[util.ZDC_P_C_K][i].row] = self.y_p[self.t, converter.converter_id, 0] \
                    - self.net.rho_v * self.ave_p[self.t, converter.converter_id, 0]    # P

                self.f[self.z[util.ZDC_Q_C_K][i].row] = self.y_p[self.t, converter.converter_id, 1] \
                    - self.net.rho_v * self.ave_p[self.t, converter.converter_id, 1]    # Q

    def update_taylor_points(self):
        super().update_taylor_points()
        if self.x == 0:
            # override defaults with net variables
            self.taylor[util.TDC_V][self.x, :] = self.ave_v[self.t, :, 0]

            if self.net.net_type == util.NET_AC_DC and len(self.net.converters) > 0:
                self.taylor[util.TDC_P_C_K][self.x, :] = self.ave_p[self.t, :, 0]
                self.taylor[util.TDC_Q_C_K][self.x, :] = self.ave_p[self.t, :, 1]
                self.taylor[util.TDC_PL_C_K][self.x, :] = self.ave_p[self.t, :, 2]

    def update_clone_variables(self):
        super().update_clone_variables()
        # build clone voltage solution array
        self.local_sol_v[self.t, :, 0] = self.taylor[util.TDC_V][self.x, :]

        # build power generated solution array
        self.local_sol_gen[self.t, 0] = self.local_sol[self.t, self.z[util.ZDC_P_GEN_Q][0].row]

        if self.net.net_type == util.NET_AC_DC and len(self.net.converters) > 0:
            self.local_sol_converter[self.t, :, 0] = self.taylor[util.TDC_P_C_K][self.x, :]      # P
            self.local_sol_converter[self.t, :, 1] = self.taylor[util.TDC_Q_C_K][self.x, :]      # Q
            self.local_sol_converter[self.t, :, 2] = self.taylor[util.TDC_PL_C_K][self.x, :]     # P_losses

    def check_local_convergence(self):
        if self.x == 0:
            return False
        else:
            # compare voltage taylor points as indication of local convergence
            current = self.taylor[util.TDC_V][self.x, :]
            previous = self.taylor[util.TDC_V][self.x-1, :]
            converged = abs(current - previous).max() < self.net.t_conv

            if self.net.net_type == util.NET_AC_DC and len(self.net.converters) > 0:
                delta = np.stack((
                    self.taylor[util.TDC_P_C_K][self.x, :] - self.taylor[util.TDC_P_C_K][self.x - 1, :],
                    self.taylor[util.TDC_Q_C_K][self.x, :] - self.taylor[util.TDC_Q_C_K][self.x - 1, :]),
                    axis=-1
                )
                converged &= abs(delta).max() < self.net.p_conv

            return converged
