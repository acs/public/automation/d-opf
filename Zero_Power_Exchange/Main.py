from mpi4py import MPI
import sys
import os
import platform
import subprocess
import time
import numpy as np
import pandas as pd
from opf import util
from opf import AcNode
from opf import DcNode
from opf import NetworkDef
# import pydevd_pycharm


def main(net_path, ruler, integrate_res):

    ruler = int(ruler)
    comm = MPI.COMM_WORLD   # intracommunicator
    size = comm.Get_size()  # will get the total number of processes
    rank = comm.Get_rank()  # will get the "name" of each process

    # if rank == ruler:
    #     port_mapping = [63096]
    #     pydevd_pycharm.settrace('localhost', port=port_mapping[0], stdoutToServer=True, stderrToServer=True)

    net = NetworkDef.NetworkDef(net_path, global_id=rank, ruler=ruler)
    identifier = "_"  # todo

    # validate mpi execution size
    if size != net.global_node_count:
        if rank == ruler:
            print("\nERROR: WRONG NUMBER OF MPI PROCESSES CALLED:\n")
            print("{} processes called, while network requires {}".format(size, net.num_nodes))
            sys.stdout.flush()
        return

    # 1. Node initialization
    if net.node_type == util.AC_NODE:
        node = AcNode.AcNode(
            integrate_res=integrate_res,
            net=net,
            comm=comm
        )
    elif net.node_type == util.DC_NODE:
        node = DcNode.DcNode(
            integrate_res=integrate_res,
            net=net,
            comm=comm
        )
    else:
        raise RuntimeError("Invalid node type")

    # Ensure everything is initialized
    comm.Barrier()

    global_convergence = False
    while node.t < net.max_iter_t and not global_convergence:
        if rank == ruler:
            util.log("t = {}".format(node.t))
            sys.stdout.flush()

        # set global iteration t for timers
        # start timing total execution time
        node.update_timing_iteration()
        node.timers['total'].start()

        # 2. Local solution to qp optimization
        #    Increments t before storing results
        node.update_clone_variables()

        # 3. Exchange clone vars to update net vars
        node.update_net_variables()

        #print(node.Flex_load_sol[node.t, :])
        #print(node.H[7][7])
        # todo handling of communication failure

        # 4. Dual Variable Update


        node.update_dual_variables()
        #node.calculate_adaptive_p()
        #node.calculate_rho()

        # 5. Check for global convergence
        global_convergence = node.check_global_convergence()

        # stop total execution time for this iteration
        node.timers['total'].stop()

    # 6. Save results

    gather_and_save_results(node, global_convergence, identifier)


    return 0

def gather_and_save_results(node, global_convergence, identifier):
    if node.net.global_id == node.net.ruler:
        util.log("Gathering results...")

    # net voltages (note t-1 was final solution)
    # received as (global_node_id, t, v_index)
    ave_v = np.ascontiguousarray(node.ave_v[0:node.t, node.net.node_id, :])
    combined_ave_v = np.zeros((node.net.global_node_count, node.t, 2))
    node.comm.Gather(ave_v, combined_ave_v, root=node.net.ruler)

    #local v
    y_v = np.ascontiguousarray(node.y_v[0:node.t, node.net.node_id, :])
    combined_y_v = np.zeros((node.net.global_node_count, node.t, 2))
    node.comm.Gather(y_v, combined_y_v, root=node.net.ruler)

    # generated power
    generation = np.ascontiguousarray(node.local_sol_gen[0:node.t, :])
    combined_generation = np.zeros((node.net.global_node_count, node.t, 2))
    node.comm.Gather(generation, combined_generation, root=node.net.ruler)

    # Penalty parameter
    penaltyparam=np.ascontiguousarray(node.penalty_parameter_rho[0:node.t, node.net.node_id])
    combined_penaltyparam = np.zeros((node.net.global_node_count, node.t))
    node.comm.Gather(penaltyparam, combined_penaltyparam, root=node.net.ruler)

    # objective function
    objective_fun = np.array(node.local_sol[0:node.t, -2])
    combined_objective_fun = np.zeros((node.net.global_node_count, node.t, 1))
    #print("objective_fun",objective_fun)
    #print("combined_objective_fun",combined_objective_fun)
    node.comm.Gather(objective_fun, combined_objective_fun, root=node.net.ruler)

    # # Power flow
    # power_flow = np.ascontiguousarray(node.Line_powerflow[0:node.t, :])
    # combined_powerflow = np.zeros((node.net.global_node_count, node.t, 2))
    # node.comm.Gather(power_flow, combined_powerflow, root=node.net.ruler)
    #
    # #Add flexible load
    # Flexible_load = np.ascontiguousarray(node.Flex_load_sol[0:node.t, :])
    # combined_Flexible_load = np.zeros((node.net.global_node_count, node.t, 2))
    # node.comm.Gather(Flexible_load, combined_Flexible_load, root=node.net.ruler)

    #Cost factor
    #cost_factor = node.net.costs_df.to_numpy()

    #combined_cost_factor = np.zeros((node.net.costs, node.t, 2))
    #node.comm.Gather(cost_factor, combined_cost_factor, root=node.net.ruler)

    total_itteration= node.t

    # gather converter data
    # collect data from DC nodes, so that power losses are available
    global_converter_data = None
    if node.net.net_type == util.NET_AC_DC:
        req = list()
        tag = 4
        global_converter_data = np.zeros((node.net.global_converter_count, node.t, 3))
        for converter_id, dc_node_id in enumerate(node.net.dc_nodes_with_converters):
            dc_node_global_id = node.net.get_dc_global_id(dc_node_id)
            if node.net.global_id == dc_node_global_id:
                # 1. send value
                req.append(node.comm.Isend(
                    np.ascontiguousarray(node.local_sol_converter[0:node.t, converter_id, :]),
                    dest=node.net.ruler,
                    tag=tag
                ))  # non-blocking

            if node.net.global_id == node.net.ruler:
                # 2. receive values
                req.append(node.comm.Irecv(
                    global_converter_data[converter_id, :, :],
                    source=dc_node_global_id,
                    tag=tag
                ))  # non-blocking
        MPI.Request.Waitall(req)

    if node.net.global_id == node.net.ruler:
        # util.log("Results gathered.")
        # util.log("Overview:")
        # util.log("Objective function: \n{}\n".format(combined_objective_fun[:, -1, :]))
        #util.log("Final voltages (re, im): \n{}\n".format(combined_ave_v[:, -1, :]))

        #
        #
        #
        #
        # calculate_cost =cost_factor[:, 0]*combined_generation[:, -1, :][:, 0]*combined_generation[:, -1, :][:, 0]+cost_factor[:, 1]*combined_generation[:, -1, :][:, 0]
        #
        # util.log("Cost of external grid $/hr: \n{}\n".format(calculate_cost[0]))
        # util.log("Final Cost $/hr: \n{}\n".format(calculate_cost.sum()))
        # util.log("Final generation (P, Q): \n{}\n".format(combined_generation[:, -1, :]))
        #
        #util.log("penalty_parameter: {}".format(combined_penaltyparam))
        #util.log(" combined_Flexible_load (P, Q): \n{}\n".format(combined_Flexible_load[:, -1, :]))

        #util.log(" combined_powerflow (P, Q): \n{}\n".format(combined_powerflow[:, -1, :]))

        # util.log("Total generation (P, Q): {}".format(np.sum(combined_generation[:, -1, :], axis=0)))
        # util.log("Total demand (P): {}".format(node.net.global_load))
        # util.log("Convergence conditions: {}".format(node.net.v_conv))
        # util.log("Maximum iterations: {}".format(node.net.max_iter_t))
        # util.log("Iterations required: {}".format(node.t))
        # util.log("Convergence reached: {}".format(global_convergence))

        #util.log("\n Timing Summary:")
        # for name, timer in node.timers.items():
        #     util.log("Total {} time {}".format(name, timer.total()))
        #     util.log("\t Avg {}".format(timer.avg()))
        #     util.log("\t Std {}".format(timer.std()))
        #     util.log("\t Min {}".format(timer.min()))
        #     util.log("\t Max {}".format(timer.max()))

        #util.log("Saving the results as .xlsx...")

        # Creating folder if it doesn't exist
        dir_name = os.path.splitext(node.net.config_file_path)[0].replace('networks', 'results')
        try:
            os.makedirs(dir_name)
            print("Directory ", dir_name, " created, results will be stored there... ")
        except FileExistsError:
            print("Directory ", dir_name, " already exists, results will be stored there...")

        # write results file
        with pd.ExcelWriter("{}/{}results.xlsx".format(dir_name, identifier)) as writer:
            #write_ac_network_data(writer, node, combined_ave_v, combined_generation, global_converter_data,combined_penaltyparam,combined_y_v)
            write_ac_network_data_simple(writer, node, combined_ave_v, combined_generation, global_converter_data,combined_penaltyparam,combined_Flexible_load,combined_powerflow,total_itteration)
            write_dc_network_data(writer, node, combined_ave_v, combined_generation, global_converter_data)
            add_converter_data(writer, node, global_converter_data)
            add_convergence_data(writer, node)

        # saving identifier
        with open("temp_identifier.txt", "w") as text_file:
            text_file.write(identifier)
        with open("temp_dirName.txt", "w") as text_file:
            text_file.write(dir_name)
        print("\nResults saved.\n")

        # # calling matlab
        # print("Calling Matlab...\n")
        # if platform.system() == "Windows":
        #     # os.system("matlab -nodesktop -nosplash -r \"plot_data\"")
        #     subprocess.Popen(
        #         "matlab -nodesktop -nosplash -r \"plot_data\"",
        #         stdout=subprocess.PIPE,
        #         stderr=subprocess.PIPE)
        #
        #     # once matlab completes, all files should be deleted
        #     # timeout after 1 min
        #     counter = 0
        #     while any("temp_" in s for s in os.listdir()):
        #         if counter > 20:
        #             print("Matlab operation has exceeded 1 min, timing out...")
        #             break
        #         else:
        #             time.sleep(3)
        #             counter += 1
        #     # os.system("del temp_*") # done by matlab script
        # else:
        #     os.system("/Applications/MATLAB_R2018b.app/bin/matlab -nodesktop -nosplash -r \"plot_data\"")
        #     # os.system("rm -f temp_*")

        print("\n\nFinished.\n\n")
        sys.stdout.flush()


def write_ac_network_data(writer, node, combined_ave_v, combined_generation, global_converter_data,combined_penaltyparam,combined_y_v):
    if node.net.ac_node_count > 0:
        ac_nodes = slice(0, node.net.ac_node_count)
        # compute voltages in polar form
        #v_mag = np.sqrt(np.sum(combined_ave_v[ac_nodes, :, :] ** 2, axis=2))
        #v_ang = np.arctan(combined_ave_v[ac_nodes, :, 1] / combined_ave_v[ac_nodes, :, 0]) * 180 / np.pi

        pd.DataFrame(np.transpose(combined_ave_v[ac_nodes, :, 0]))\
            .to_excel(writer, sheet_name="Net_re")

        pd.DataFrame(np.transpose(combined_ave_v[ac_nodes, :, 1]))\
            .to_excel(writer, sheet_name="Net_im")

        pd.DataFrame(np.transpose(combined_y_v[ac_nodes, :, 0])) \
            .to_excel(writer, sheet_name="Local_re")

        pd.DataFrame(np.transpose(combined_y_v[ac_nodes, :, 1])) \
            .to_excel(writer, sheet_name="Local_im")

        # pd.DataFrame(np.transpose(v_mag[:,-1]))\
        #     .to_excel(writer, sheet_name="ac_v_mag")
        #
        # pd.DataFrame(np.transpose(v_ang[:,-1]))\
        #     .to_excel(writer, sheet_name="ac_v_ang")

        pd.DataFrame(np.transpose(combined_generation[ac_nodes, :, 0]))\
            .to_excel(writer, sheet_name="ac_p_gen")

        pd.DataFrame(np.transpose(combined_generation[ac_nodes,:, 1]))\
            .to_excel(writer, sheet_name="ac_q_gen")

        pd.DataFrame(np.ones(node.t) * node.net.ac_load[0])\
            .to_excel(writer, sheet_name="ac_p_load")

        pd.DataFrame(np.ones(node.t) * node.net.ac_load[1])\
            .to_excel(writer, sheet_name="ac_q_load")

        pd.DataFrame(np.transpose(combined_penaltyparam)) \
            .to_excel(writer, sheet_name="combined_penaltyparam")




def write_ac_network_data_simple(writer, node, combined_ave_v, combined_generation, global_converter_data,combined_penaltyparam, combined_Flexible_load,combined_powerflow,total_itteration):
    if node.net.ac_node_count > 0:
        ac_nodes = slice(0, node.net.ac_node_count)
        # compute voltages in polar form
        v_mag = np.sqrt(np.sum(combined_ave_v[ac_nodes, :, :] ** 2, axis=2))
        v_ang = np.arctan(combined_ave_v[ac_nodes, :, 1] / combined_ave_v[ac_nodes, :, 0]) * 180 / np.pi
        timess=[]

        for name, timer in node.timers.items():
            timess.append(timer.total())
            #util.log("Total {} time {}".format(name, timer.total()))

        pd.DataFrame({'ac_v_re': combined_ave_v[ac_nodes, -1, 0],
                      'ac_v_im': combined_ave_v[ac_nodes, -1, 1],
                      "ac_v_mag":v_mag[:,-1],
                      "ac_v_ang":v_ang[:,-1],
                      "ac_p_gen":[float(i) for i in combined_generation[ac_nodes, -1, 0]],
                      "ac_q_gen":[float(i) for i in combined_generation[ac_nodes, -1, 1]],
                      "ac_p_flex":[float(i) for i in combined_Flexible_load[ac_nodes, -1, 0]],
                      "ac_q_flex": [float(i) for i in combined_Flexible_load[ac_nodes, -1, 1]],
                      "ac_p_powerflow": combined_powerflow[ac_nodes, -1, 0],
                      "ac_q_powerflow": combined_powerflow[ac_nodes, -1, 1]
                      }).to_excel(writer, sheet_name="Results")

        #load network data again
        df = pd.read_excel(
            io=os.path.join(node.net.config_file_path, "ac_data.xlsx"),
            sheet_name=['bus', 'branch', 'gen', 'flex'],
            header=0,
            index_col=None,
            dtype=float
        )
        total_internal_gen_p_max = []
        total_internal_gen_p_min = []

        total_internal_gen_q_max = []
        total_internal_gen_q_min = []

        total_external_gen_p_max = []
        total_external_gen_p_min = []

        total_external_gen_q_max = []
        total_external_gen_q_min = []

        total_max_flexible_load_p = []
        total_max_flexible_load_q = []

        Cost_linear_internal_gen = []
        Cost_quadratic_internal_gen = []

        # generator data
        for row, gen in df['gen'].iterrows():
            bus_id = int(gen['GEN_BUS'] - 1)

            total_internal_gen_p_max.append(gen['PMAX'])
            total_internal_gen_p_min.append(gen['PMIN'])

            total_internal_gen_q_max.append(gen['QMAX'])
            total_internal_gen_q_min.append(gen['QMIN'])

            Cost_linear_internal_gen.append(gen['COST_B'])
            Cost_quadratic_internal_gen.append(gen['COST_A'])

        Cost_linear_external_gen=Cost_linear_internal_gen[0]
        Cost_quadratic_external_gen=Cost_quadratic_internal_gen[0]

        del Cost_linear_internal_gen[0]
        del Cost_quadratic_internal_gen[0]


        total_external_gen_p_max = total_internal_gen_p_max[0]
        total_external_gen_p_min = total_internal_gen_p_min[0]
        total_external_gen_q_max =total_internal_gen_q_max[0]
        total_external_gen_q_min =total_internal_gen_q_min[0]

        total_internal_gen_p_max=sum(total_internal_gen_p_max)-total_external_gen_p_max
        total_internal_gen_p_min=sum(total_internal_gen_p_min)-total_external_gen_p_min

        total_internal_gen_q_max=sum(total_internal_gen_q_max)-total_external_gen_q_max
        total_internal_gen_q_min = sum(total_internal_gen_q_min) - total_external_gen_q_min
        costs_flex_linear=[]
        costs_flex_quadratic=[]
        # Flexible load
        costs_flex_linear = []
        costs_flex_quadratic = []
        Flexible_location=[]
        for row, flex in df['flex'].iterrows():
            total_max_flexible_load_p.append(flex['PMAX'])
            total_max_flexible_load_q.append(flex['QMAX'])

            #if bus_id == node.net.node_id:  # A quadratic, B Linear.
            costs_flex_linear.append(flex['COST_B'])
            costs_flex_quadratic.append(flex['COST_A'])
            Flexible_location.append(flex['GEN_BUS'])

        flexible_load_p=total_max_flexible_load_p
        flexible_load_q = total_max_flexible_load_q
        total_max_flexible_load_p=sum(total_max_flexible_load_p)
        total_max_flexible_load_q=sum(total_max_flexible_load_q)


        pd.DataFrame({'Total_fixed_P_Load': [node.net.global_load[0]],
                      'Total_fixed_Q_Load': [node.net.global_load[1]],
                      'Total_internal_gen_p_max': [total_internal_gen_p_max],
                      'Total_internal_gen_p_min': [total_internal_gen_p_min],
                      'Total_internal_gen_q_max': [total_internal_gen_q_max],
                      'Total_internal_gen_q_min': [total_internal_gen_q_min],
                      'Total_external_gen_p_max': [total_external_gen_p_max],
                      'Total_external_gen_p_min': [total_external_gen_p_min],
                      'Total_external_gen_q_max': [total_external_gen_q_max],
                      'Total_external_gen_q_min': [total_external_gen_q_min],

                      'Cost_linear_external_gen': [Cost_linear_external_gen],
                      'Cost_quadratic_external_gen': [Cost_quadratic_external_gen],

                      'Cost_linear_internal_gen': [Cost_linear_internal_gen],
                      'Cost_quadratic_internal_gen': [Cost_quadratic_internal_gen],

                      'Total_max_flexible_load_p': [total_max_flexible_load_p],
                      'Total_max_flexible_load_q': [total_max_flexible_load_q],

                      'max_flexible_load_p': [flexible_load_p],
                      'max_flexible_load_q': [flexible_load_q],
                      'Cost_linear_flexible_load_': [costs_flex_linear],
                      'Cost_quadratic_flexible_load_': [costs_flex_quadratic],
                      'Flexible_load_location': [Flexible_location],
                      }).T.to_excel(writer, sheet_name="Network_parameter")

        pd.DataFrame({'Flexible curtailment': node.net.curtailment,
                      'Rho_v': [node.net.rho_v],
                      'number_external_iterations': [node.net.max_iter_t],
                      'number_internal_iterations': [node.net.max_iter_x],
                      'voltage_convergence_criterion': [node.net.v_conv],
                      'taylor_convergence_criterion': [node.net.t_conv],
                      "Power balance criterion":[node.net.power_balance]
                      }).T.to_excel(writer, sheet_name="Algo_parameter")
        pd.DataFrame({"Time": timess[2],
                      "Iterations": [total_itteration]
                      }).T.to_excel(writer, sheet_name="Computational Cost")
        # pd.DataFrame(np.transpose(combined_penaltyparam)) \
        #     .to_excel(writer, sheet_name="combined_penaltyparam")

def write_dc_network_data(writer, node, combined_ave_v, combined_generation, global_converter_data):
    if node.net.dc_node_count > 0:
        dc_nodes = slice(node.net.ac_node_count, node.net.global_node_count)

        pd.DataFrame(np.transpose(combined_ave_v[dc_nodes, :, 0]))\
            .to_excel(writer, sheet_name="dc_v")

        pd.DataFrame(np.transpose(combined_generation[dc_nodes, :, 0]))\
            .to_excel(writer, sheet_name="dc_p_gen")

        pd.DataFrame(np.ones(node.t) * node.net.dc_load[0])\
            .to_excel(writer, sheet_name="dc_p_load")


def add_converter_data(writer, node, global_converter_data):
    if node.net.net_type == util.NET_AC_DC:

        pd.DataFrame(np.transpose(global_converter_data[:, :, 0]))\
            .to_excel(writer, sheet_name="conv_p")

        pd.DataFrame(np.transpose(global_converter_data[:, :, 1]))\
            .to_excel(writer, sheet_name="conv_q")

        pd.DataFrame(np.transpose(global_converter_data[:, :, 2]))\
            .to_excel(writer, sheet_name="conv_p_loss")


def add_convergence_data(writer, node):
    convergence_data = [
        node.net_var_convergence_sum[-1],
        node.net.global_node_count
    ]

    pd.DataFrame(convergence_data)\
        .to_excel(writer, sheet_name="convergence")


if __name__ == "__main__":
    if len(sys.argv) < 4:
        raise RuntimeError("Insufficient inputs provided to Main.py", str(sys.argv))

    main(sys.argv[1], sys.argv[2], sys.argv[3])

