# **D-OPF** <br/> _Distributed Optimal Power Flow_


[<img src="docs/access-gagraphic-3010876.jpg">](https://ieeexplore.ieee.org/document/9145545)

_Click on the image to go to the document page_


The distributed OPF algorithm solves the OPF problem in AC, DC or AC/DC systems. The distributed structure in nodal formulation allows for a scalable and modular OPF tool. The algorithm can solve the OPF problem in systems of any size and any topology (radial or meshed). The modularity gives the possibility to integrate easily distributed energy resources (DER), which can participate in the power dispatch. In addition, specific system operation, as desired by the system operator, can be modelled and simulated.
The provided Python code simulates the OPF problem in AC, DC, or AC/DC systems. The README.md file provides information of the structure of the code in the different Python files. In addition, two examples of code for the modified OPF problem are provided, where DER are integrated, in particular flexible loads, and specific system operation scenarios are modelled. Specifically, one example refers to the system operation scenario of zero power exchange with the external grid, whereas the other example refers to the scenario of the power injection to the external grid by a desired amount. A paper is provided in the link, as example of the OPF algorithm for DC systems, where different DER are integrated, e.g. renewable energy sources, storage systems and flexible loads, which participate in the power dispatch not only according to their operational costs, but also according to their characteristics during the system operation.

Paper link: https://ieeexplore.ieee.org/document/9145545

## Copyright
2015-2023, Asimenia Korompili (ACS) <br/>
2023, Institute for Automation of Complex Power Systems, EONERC

## License

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.

## Funding
<img alt="PLATONE" style="border-width:0" src="docs/platone_logo.png" height="63"/></a>&nbsp; 
<a rel="funding" href="https://cordis.europa.eu/project/id/864300"><img alt="H2020" style="border-width:0" src="https://hyperride.eu/wp-content/uploads/2020/10/europa_flag_low.jpg" height="63"/></a><br />
This work was supported by <a rel="Platone" href="https://platone-h2020.eu/">PLATform for Operation of distribution NEtworks </a> (Platone), funded by the European Union's Horizon 2020 research and innovation programme under <a rel="H2020" href="https://cordis.europa.eu/project/id/864300"> grant agreement No. 864300</a>.

## Contact

[![EONERC ACS Logo](docs/eonerc_logo.png)](http://www.acs.eonerc.rwth-aachen.de)

- [Asimenia Korompili, M.Sc.](mailto:akorompili@eonerc.rwth-aachen.de)

[Institute for Automation of Complex Power Systems (ACS)](http://www.acs.eonerc.rwth-aachen.de)  
[E.ON Energy Research Center (EONERC)](http://www.eonerc.rwth-aachen.de)  
[RWTH University Aachen, Germany](http://www.rwth-aachen.de)
