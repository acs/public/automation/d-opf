# Imports
import numpy as np
from . import util
from . import Node
from . import NetworkDef
from . import Equality
from . import Inequality


class AcNode(Node.Node):

    def __init__(self, integrate_res, net: NetworkDef, comm):
        super().__init__(integrate_res=integrate_res, net=net, comm=comm)

    def init_state_vector(self):

        # state var id : taylor id
        neighbour_vars = {
            util.ZAC_V_I_RE: util.TAC_V_RE,  # Vi Neighbour clone voltages (real)
            util.ZAC_V_I_IM: util.TAC_V_IM,  # Vi Neighbour clone voltage (imaginary)
            util.ZAC_I_QI_RE: util.TAC_I_RE,  # I_qi Neighbour branch currents (real)
            util.ZAC_I_QI_IM: util.TAC_I_IM,  # I_qi Neighbour branch currents (imaginary)
            util.ZAC_P_QI: util.TAC_P,  # P_qi Neighbour branch real power
            util.ZAC_Q_QI: util.TAC_Q,  # Q_qi Neighbour branch reactive power
        }

        row = super().add_neighbour_state_vars(neighbour_vars)

        if self.net.net_type == util.NET_AC_DC:
            # state var id : taylor id
            converter_vars = {
                util.ZAC_P_C_K: util.TAC_P_C_K,   # Converter active power injected
                util.ZAC_Q_C_K: util.TAC_Q_C_K    # Converter reactive power injected
            }

            row = super().add_converter_state_vars(converter_vars, row=row)

        # state var id : taylor id
        node_vars = {
            util.ZAC_P_GEN_Q: util.TAC_GEN_P,
            util.ZAC_Q_GEN_Q: util.TAC_GEN_Q,
            #Add P_flex_load_Q// Done
            util.ZAC_P_FLEX_Q: util.TAC_FLEX_P,
            util.ZAC_Q_FLEX_Q: util.TAC_FLEX_Q,

            #Add Q_flex_load_Q //Done

            util.ZAC_P_INJ_Q: util.TAC_P,
            util.ZAC_Q_INJ_Q: util.TAC_Q,
            util.ZAC_I_Q_RE: util.TAC_I_RE,  # Own real current injected
            util.ZAC_I_Q_IM: util.TAC_I_IM,  # Own imaginary current injected
            util.ZAC_V_Q_RE: util.TAC_V_RE,  # Own clone voltage (real)
            util.ZAC_V_Q_IM: util.TAC_V_IM,  # Own clone voltage (imaginary)
        }

        # add and store number of state variables
        self.num_state_vars = super().add_node_state_vars(node_vars, row=row)

    def init_equalities(self):
        self.equalities.append(Equality.ACNodePowerBalanceEq(name='S_q', node=self))
        self.equalities.append(Equality.ACNodeCurrentEq(name='I_q', node=self))
        self.equalities.append(Equality.ACBranchCurrentEq(name='I_qi', node=self))
        self.equalities.append(Equality.ACBranchPowerEq(name='S_qi', node=self))
        self.equalities.append(Equality.ACNodePowerEq(name='S_q_inj', node=self))

        # convert to numpy arrays now that all constraints are known
        self.A_eq = np.asarray(self.A_eq, dtype=np.double)
        self.b_eq = np.asarray(self.b_eq, dtype=np.double)

    def init_inequalities(self):

        # Neighbour quantity constraints
        self.inequalities.append(Inequality.AcNonConvexMinConstraint(
            name='v_i_min', node=self, z_re=util.ZAC_V_I_RE, z_im=util.ZAC_V_I_IM,
            mag_min=util.L_V_MIN))

        self.inequalities.append(Inequality.AcConvexMaxConstraint(
            name='v_i_max', node=self, z_re=util.ZAC_V_I_RE, z_im=util.ZAC_V_I_IM,
            re_max=util.L_V_MAX, im_max=util.L_V_MAX, mag_max=util.L_V_MAX))

        self.inequalities.append(Inequality.AcConvexMaxConstraint(
            name='branch_current_max', node=self, z_re=util.ZAC_I_QI_RE, z_im=util.ZAC_I_QI_IM,
            re_max=util.L_I_FLOW_MAX, im_max=util.L_I_FLOW_MAX, mag_max=util.L_I_FLOW_MAX))

        self.inequalities.append(Inequality.AcConvexMaxConstraint(
            name='branch_power_max', node=self, z_re=util.ZAC_P_QI, z_im=util.ZAC_Q_QI,
            re_max=util.L_S_FLOW_MAX, im_max=util.L_S_FLOW_MAX, mag_max=util.L_S_FLOW_MAX))

        # Own node constraints
        self.inequalities.append(Inequality.MinConstraint(
            name='p_gen_min', node=self, z=util.ZAC_P_GEN_Q, mag_min=util.L_P_GEN_MIN))

        self.inequalities.append(Inequality.MinConstraint(
            name='q_gen_min', node=self, z=util.ZAC_Q_GEN_Q, mag_min=util.L_Q_GEN_MIN))

        self.inequalities.append(Inequality.AcConvexMaxConstraint(
            name='s_gen_max', node=self, z_re=util.ZAC_P_GEN_Q, z_im=util.ZAC_Q_GEN_Q,
            re_max=util.L_P_GEN_MAX, im_max=util.L_Q_GEN_MAX, mag_max=util.L_S_GEN_MAX))

        #Copy for flexible load, change the variable name and parameter name accordingly

        self.inequalities.append(Inequality.AcConvexMaxConstraint(
            name='s_flex_max', node=self, z_re=util.ZAC_P_FLEX_Q, z_im=util.ZAC_Q_FLEX_Q,
            re_max=util.L_P_FLEX_MAX, im_max=util.L_Q_FLEX_MAX, mag_max=util.L_S_FLEX_MAX))

        self.inequalities.append(Inequality.MinConstraint(
            name='p_flex_min', node=self, z=util.ZAC_P_FLEX_Q, mag_min=util.L_P_FLEX_MIN))

        self.inequalities.append(Inequality.MinConstraint(
            name='q_flex_min', node=self, z=util.ZAC_Q_FLEX_Q, mag_min=util.L_Q_FLEX_MIN))


        #
        # self.inequalities.append(Inequality.MaxConstraint(
        #     name='p_gen_max', node=self, z=util.ZAC_P_GEN_Q, mag_max=util.L_P_GEN_MAX))
        #
        # self.inequalities.append(Inequality.MaxConstraint(
        #     name='q_gen_max', node=self, z=util.ZAC_Q_GEN_Q, mag_max=util.L_Q_GEN_MAX))

        if self.net.net_type == util.NET_AC_DC:
            for converter in self.net.converters:
                self.inequalities.append(Inequality.ConverterConvexMaxConstraint(
                    name='C_k_s_max', node=self, z_re=util.ZAC_P_C_K, z_im=util.ZAC_Q_C_K))

        self.inequalities.append(Inequality.AcConvexMaxConstraint(
            name='node_power_injection_max', node=self, z_re=util.ZAC_P_INJ_Q, z_im=util.ZAC_Q_INJ_Q,
            re_max=util.L_P_INJ_MAX, im_max=util.L_Q_INJ_MAX, mag_max=util.L_S_INJ_MAX))

        self.inequalities.append(Inequality.AcConvexMaxConstraint(
            name='node_current_injection_max', node=self, z_re=util.ZAC_I_Q_RE, z_im=util.ZAC_I_Q_IM,
            re_max=util.L_I_INJ_MAX, im_max=util.L_I_INJ_MAX, mag_max=util.L_I_INJ_MAX))

        self.inequalities.append(Inequality.AcConvexMaxConstraint(
            name='v_q_max', node=self, z_re=util.ZAC_V_Q_RE, z_im=util.ZAC_V_Q_IM,
            re_max=util.L_V_MAX, im_max=util.L_V_MAX, mag_max=util.L_V_MAX))

        self.inequalities.append(Inequality.AcNonConvexMinConstraint(
            name='v_q_min', node=self, z_re=util.ZAC_V_Q_RE, z_im=util.ZAC_V_Q_IM,
            mag_min=util.L_V_MIN))

        # convert to numpy arrays now that all constraints are known
        self.A_ineq = np.asarray(self.A_ineq, dtype=np.double)
        self.b_ineq = np.asarray(self.b_ineq, dtype=np.double)

    def init_optimization_matrices(self):
        # H Matrix, squared terms of the objective function (1/2 * z.T * H * z) (the whole matrix is constant)
        # penalty for clone variables vs. net variable
        def add_rho_terms(state_var, rho):
            for z in self.z[state_var]:
                self.H[z.row, z.row] = rho

        add_rho_terms(util.ZAC_V_I_RE, self.net.rho_v)
        add_rho_terms(util.ZAC_V_I_IM, self.net.rho_v)
        add_rho_terms(util.ZAC_V_Q_RE, self.net.rho_v)
        add_rho_terms(util.ZAC_V_Q_IM, self.net.rho_v)

        if self.net.net_type == util.NET_AC_DC:
            add_rho_terms(util.ZAC_P_C_K, self.net.rho_p)
            add_rho_terms(util.ZAC_Q_C_K, self.net.rho_p)
        self.H[self.z[util.ZAC_P_GEN_Q][0].row, self.z[util.ZAC_P_GEN_Q][0].row] = 2 * self.net.costs[0]



        if self.net.node_id == 0:
            # Pgen costs ki2) for this node
            self.f[self.z[util.ZAC_P_GEN_Q][0].row] = - self.net.costs[1]  # No change
        else:
            self.f[self.z[util.ZAC_P_GEN_Q][0].row] = self.net.costs[1]

        #Flex
        self.H[self.z[util.ZAC_P_FLEX_Q][0].row, self.z[util.ZAC_P_FLEX_Q][0].row] = 2 * self.net.costs_flex[0] *-1
        self.f[self.z[util.ZAC_P_FLEX_Q][0].row] = self.net.costs_flex[1] * -1


    def update_optimization_matrices(self):
        # helper function to update voltage penalty term
        def update_voltage_penalty(state_var, is_imag):
            for z in self.z[state_var]:
                self.f[z.row] = self.y_v[self.t, z.node_id, is_imag] \
                                - self.net.rho_v * self.ave_v[self.t, z.node_id, is_imag]

        update_voltage_penalty(util.ZAC_V_I_RE, 0)
        update_voltage_penalty(util.ZAC_V_I_IM, 1)
        update_voltage_penalty(util.ZAC_V_Q_RE, 0)
        update_voltage_penalty(util.ZAC_V_Q_IM, 1)

        if self.net.net_type == util.NET_AC_DC:
            # converter power penalty
            for i, converter in enumerate(self.net.converters):
                self.f[self.z[util.ZAC_P_C_K][i].row] = self.y_p[self.t, converter.converter_id, 0] \
                    - self.net.rho_p * self.ave_p[self.t, converter.converter_id, 0]    # P

                self.f[self.z[util.ZAC_Q_C_K][i].row] = self.y_p[self.t, converter.converter_id, 1] \
                    - self.net.rho_p * self.ave_p[self.t, converter.converter_id, 1]    # Q

    def update_taylor_points(self):
        super().update_taylor_points()
        if self.x == 0:
            # override default with net variables
            self.taylor[util.TAC_V_RE][self.x, :] = self.ave_v[self.t, :, 0]
            self.taylor[util.TAC_V_IM][self.x, :] = self.ave_v[self.t, :, 1]

            if self.net.net_type == util.NET_AC_DC and len(self.net.converters) > 0:
                self.taylor[util.TAC_P_C_K][self.x, :] = self.ave_p[self.t, :, 0]
                self.taylor[util.TAC_Q_C_K][self.x, :] = self.ave_p[self.t, :, 1]

    def update_clone_variables(self):
        super().update_clone_variables()
        # build clone voltage solution array
        self.local_sol_v[self.t] = np.stack((
            self.taylor[util.TAC_V_RE][self.x, :],
            self.taylor[util.TAC_V_IM][self.x, :]),
            axis=-1
        )

        # build power generated solution array+
        #print(self.local_sol[self.t, self.z[util.ZAC_P_GEN_Q][0].row])
        self.local_sol_gen[self.t] = np.array([
            self.local_sol[self.t, self.z[util.ZAC_P_GEN_Q][0].row],
            self.local_sol[self.t, self.z[util.ZAC_Q_GEN_Q][0].row]
            #self.local_sol[self.t, self.z[util.ZAC_P_FLEX_Q][0].row],
            #self.local_sol[self.t, self.z[util.ZAC_Q_FLEX_Q][0].row]
        ])
        #Add P & Q of flex load. //Done

        self.Flex_load_sol[self.t] = np.array([
            #self.Flex_load[self.t, self.z[util.ZAC_P_GEN_Q][0].row],
            #self.Flex_load[self.t, self.z[util.ZAC_Q_GEN_Q][0].row]
            self.local_sol[self.t, self.z[util.ZAC_P_FLEX_Q][0].row],
            self.local_sol[self.t, self.z[util.ZAC_Q_FLEX_Q][0].row]
        ])

        #for powerflow
        #print(self.Line_powerflow_sol[self.t, self.z[util.ZAC_P_QI][0].row])
        self.Line_powerflow[self.t] = np.array([
            self.local_sol[self.t, self.z[util.ZAC_P_QI][0].row],
            self.local_sol[self.t, self.z[util.ZAC_Q_QI][0].row]
        ])


        if self.net.net_type == util.NET_AC_DC and len(self.net.converters) > 0:
            self.local_sol_converter[self.t, :, 0] = self.taylor[util.TDC_P_C_K][self.x, :]
            self.local_sol_converter[self.t, :, 1] = self.taylor[util.TDC_Q_C_K][self.x, :]

    def check_local_convergence(self):
        if self.x == 0:
            return False
        else:
            # compare voltage taylor points as indication of local convergence

            delta = np.stack((
                self.taylor[util.TAC_V_RE][self.x, :] - self.taylor[util.TAC_V_RE][self.x-1, :],
                self.taylor[util.TAC_V_IM][self.x, :] - self.taylor[util.TAC_V_IM][self.x-1, :]),
                axis=-1
            )
            converged = abs(delta).max() < self.net.t_conv

            if self.net.net_type == util.NET_AC_DC and len(self.net.converters) > 0:
                delta = np.stack((
                    self.taylor[util.TAC_P_C_K][self.x, :] - self.taylor[util.TAC_P_C_K][self.x - 1, :],
                    self.taylor[util.TAC_Q_C_K][self.x, :] - self.taylor[util.TAC_Q_C_K][self.x - 1, :]),
                    axis=-1
                )
                converged &= abs(delta).max() < self.net.p_conv

            return converged
