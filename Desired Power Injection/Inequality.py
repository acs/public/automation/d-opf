import numpy as np
from . import Node
from . import AcNode


class Inequality:

    # Base class for equality constraints
    # They should be initialized only from the Node's init_equalities() function
    def __init__(self, name, node: Node, num_rows=1):
        self.name = name
        self.node = node
        self.rows = range(len(self.node.A_ineq), len(self.node.A_ineq) + num_rows)

        # Pre allocate rows in A_eq and b_eq
        for _ in self.rows:
            self.node.A_ineq.append([0] * self.node.num_state_vars)
            self.node.b_ineq.append(0)

    # template function
    def update(self): pass


# Donut approximation formulation of non-convex min constraints
class AcNonConvexMinConstraint(Inequality):

    def __init__(self, name, node: AcNode, z_re, z_im, mag_min):
        if len(node.z[z_re]) != len(node.z[z_im]):
            raise RuntimeError("Invalid state variables for limit ", name)

        super().__init__(name=name, node=node, num_rows=len(node.z[z_re]))
        self.n_vars = len(node.z[z_re])
        self.z_re = z_re
        self.z_im = z_im

        # extract minimum limits for the respective state variables
        node_ids = [state_var.node_id for state_var in self.node.z[z_re]]
        self.min = self.node.net.limits[mag_min][node_ids]


    def update(self):
        super().update()

        for j in range(self.n_vars):
            z_re = self.node.z[self.z_re][j]        # State Variable for real value
            z_im = self.node.z[self.z_im][j]        # State Variable for imaginary value
            t_re = self.node.taylor[z_re.taylor][self.node.x, z_re.node_id]     # Taylor exp point for re value
            t_im = self.node.taylor[z_im.taylor][self.node.x, z_im.node_id]     # Taylor exp point for im value

            if t_re == 0:
                a = 0
                b = np.sign(t_im)
                c = self.min[j]
            else:
                a = np.sign(t_re) * self.min[j] / np.sqrt(1 + (t_im / t_re) ** 2)
                b = a * (t_im / t_re)
                c = self.min[j] ** 2

            self.node.A_ineq[self.rows[j]][z_re.row] = -a  # re
            self.node.A_ineq[self.rows[j]][z_im.row] = -b  # im
            self.node.b_ineq[self.rows[j]] = -c  # sqrt(re^2 + im^2)


# Donut approximation formulation of convex max constraints
class AcConvexMaxConstraint(Inequality):

    def __init__(self, name, node: AcNode, z_re, z_im, re_max, im_max, mag_max):

        if len(node.z[z_re]) != len(node.z[z_im]):
            raise RuntimeError("Invalid state variables for limit ", name)

        super().__init__(name=name, node=node, num_rows=3*len(node.z[z_re]))
        self.n_vars = len(node.z[z_re])
        self.z_re = z_re
        self.z_im = z_im

        # extract minimum limits for the respective state variables
        node_ids = [state_var.node_id for state_var in self.node.z[z_re]]
        self.re_max = self.node.net.limits[re_max][node_ids]
        self.im_max = self.node.net.limits[im_max][node_ids]
        self.mag_max = self.node.net.limits[mag_max][node_ids]

    def update(self):
        super().update()

        for j in range(self.n_vars):
            z_re = self.node.z[self.z_re][j]        # State Variable for real value
            z_im = self.node.z[self.z_im][j]        # State Variable for imaginary value
            t_re = self.node.taylor[z_re.taylor][self.node.x, z_re.node_id]     # Taylor exp point for re value
            t_im = self.node.taylor[z_im.taylor][self.node.x, z_im.node_id]     # Taylor exp point for im value

            if self.mag_max[j] == 0:
                a = 1  # a + b <= 0
                b = 1
                c = 0
                a1 = -1  # a >= 0
                b1 = -1  # b >= 0
                c1 = 0
                c2 = 0
            else:
                if t_re == 0:
                    a = 0
                    b = np.sign(t_im)
                    c = self.mag_max[j]
                else:
                    a = np.sign(t_re) * self.mag_max[j] / np.sqrt(1 + (t_im / t_re) ** 2)
                    b = a * (t_im / t_re)
                    c = self.mag_max[j] ** 2
                a1 = np.sign(t_re)
                b1 = np.sign(t_im)
                c1 = self.re_max[j]
                c2 = self.im_max[j]

            # combined linearized equality
            self.node.A_ineq[self.rows[j], z_re.row] = a  # Re
            self.node.A_ineq[self.rows[j], z_im.row] = b  # Im
            self.node.b_ineq[self.rows[j]] = c  # sqrt(Re^2 + Im^2)

            # linearized equality on real value
            self.node.A_ineq[self.rows[j] + 1 * self.n_vars, z_re.row] = a1  # Re
            self.node.b_ineq[self.rows[j] + 1 * self.n_vars] = c1   # Re

            # linearized equality on imaginary value
            self.node.A_ineq[self.rows[j] + 2 * self.n_vars, z_im.row] = b1  # Im
            self.node.b_ineq[self.rows[j] + 2 * self.n_vars] = c2   # Im


# Donut approximation formulation of convex max constraints
class ConverterConvexMaxConstraint(AcConvexMaxConstraint):

    def __init__(self, name, node: Node, z_re, z_im):

        if len(node.z[z_re]) != len(node.z[z_im]):
            raise RuntimeError("Invalid state variables for limit ", name)

        Inequality.__init__(self, name=name, node=node, num_rows=3*len(node.net.converters))
        self.n_vars = len(self.node.net.converters)
        self.z_re = z_re
        self.z_im = z_im
        self.mag_max = [converter.s_max for converter in self.node.net.converters]
        self.re_max = self.mag_max
        self.im_max = self.mag_max

    def update(self):
        super().update()


# single quantity min
# constant
class MinConstraint(Inequality):

    def __init__(self, name, node: Node, z, mag_min, multiplier=1):
        super().__init__(name=name, node=node, num_rows=len(node.z[z]))
        self.n_vars = len(node.z[z])
        self.z = z

        # extract minimum limits for the respective state variables
        node_ids = [state_var.node_id for state_var in self.node.z[z]]
        self.min = self.node.net.limits[mag_min][node_ids]

        for j in range(self.n_vars):
            # -var <= -min
            self.node.A_ineq[self.rows[j]][self.node.z[self.z][j].row] = -1
            self.node.b_ineq[self.rows[j]] = -multiplier * self.min[j]


# single quantity max
# constant
class MaxConstraint(Inequality):

    def __init__(self, name, node: Node, z, mag_max):
        super().__init__(name=name, node=node, num_rows=len(node.z[z]))
        self.n_vars = len(node.z[z])
        self.z = z

        # extract minimum limits for the respective state variables
        node_ids = [state_var.node_id for state_var in self.node.z[z]]
        self.max = self.node.net.limits[mag_max][node_ids]

        for j in range(self.n_vars):
            # var <= max
            self.node.A_ineq[self.rows[j]][self.node.z[self.z][j].row] = 1
            self.node.b_ineq[self.rows[j]] = self.max[j]

